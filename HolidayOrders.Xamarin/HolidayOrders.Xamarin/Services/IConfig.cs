﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HolidayOrders.Xamarin.Services
{
    public interface IConfig
    {
        string RestBaseAdress { get; }
        string GetAllHolidaysFunctionName { get; }
        string GetItemsByOrderIdFunctionName { get; }
        string GetOrdersByHolidayIdFunctionName { get; }
        string GetAllHolidaysFunctionCode { get; }
        string GetItemsByOrderIdFunctionCode { get; }
        string GetOrdersByHolidayIdFunctionCode { get; }


    }
}
