﻿using HolidayOrders.Xamarin.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HolidayOrders.Xamarin.Services
{
    public class RestDataProvider : IDataProvider
    {
        private IRestClient _restClient;
        private IConfig _config;

        public RestDataProvider(IConfig config)
        {
            _config = config;
            _restClient = new RestClient(config.RestBaseAdress);
        }
        public async Task<IEnumerable<Holiday>> GetAllHolidaysAsync()
        {
            return await Task.Run(() =>
            {
                var requst = new RestRequest(_config.GetAllHolidaysFunctionName)
                .AddParameter("code", _config.GetAllHolidaysFunctionCode);

                var result = _restClient.Get(requst);
                if (result.IsSuccessful)
                {
                    var jsonContent = result.Content;
                    return JsonConvert.DeserializeObject<IEnumerable<Holiday>>(jsonContent);
                }
                return null;
            });
        }


        public async Task<IEnumerable<Item>> GetAllItemsForOrderIdAsync(int orderId)
        {
            return await Task.Run(() =>
            {
                var requst = new RestRequest(_config.GetItemsByOrderIdFunctionName)
                    .AddParameter("code", _config.GetItemsByOrderIdFunctionCode)
                    .AddParameter("OrderId", orderId);

                var result = _restClient.Get(requst);
                if (result.IsSuccessful)
                {
                    var jsonContent = result.Content;
                    return JsonConvert.DeserializeObject<IEnumerable<Item>>(jsonContent);
                }
                return null;
            });

        }

        public async Task<IEnumerable<Order>> GetOrdersForHolidayIdAsync(int holidayId)
        {
            return await Task.Run(() =>
            {
                var requst = new RestRequest(_config.GetOrdersByHolidayIdFunctionName)
        .AddParameter("code", _config.GetOrdersByHolidayIdFunctionCode)
        .AddParameter("HolidayId", holidayId);

                var result = _restClient.Get(requst);
                if (result.IsSuccessful)
                {
                    var jsonContent = result.Content;
                    return JsonConvert.DeserializeObject<IEnumerable<Order>>(jsonContent);
                }
                return null;
            });
        }
    }
}
