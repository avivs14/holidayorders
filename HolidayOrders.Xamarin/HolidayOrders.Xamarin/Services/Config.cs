﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HolidayOrders.Xamarin.Services
{
    public class Config : IConfig
    {
        public string RestBaseAdress => @"https://holidayorderfunctions.azurewebsites.net/api/";
        public string GetAllHolidaysFunctionName => "GetAllHolidaysFunction";
        public string GetItemsByOrderIdFunctionName => "GetItemsByOrderIdFunction";
        public string GetOrdersByHolidayIdFunctionName => "GetOrdersByHolidayIdFunction";
        public string GetAllHolidaysFunctionCode => "WOnvHZsvL9aMGlwuR1RepzJ6isTAii/LnHnAVXvxQT1dQNMBgxTOCQ==";
        public string GetItemsByOrderIdFunctionCode => "ioTTzP3gmoy1HS95HhnN6XSCi11Wbs/n0WsXdagd10irNjzLb7GAUA==";
        public string GetOrdersByHolidayIdFunctionCode => "4kZW7zcmMb6Kv6cjrBjwJaBZCNEdaUweRkq07jnAAy7R2xarxt0ywQ==";
    }
}
