﻿using HolidayOrders.Xamarin.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HolidayOrders.Xamarin.Services
{
    public interface IDataProvider
    {
        Task<IEnumerable<Holiday>> GetAllHolidaysAsync();
        Task<IEnumerable<Order>> GetOrdersForHolidayIdAsync(int holidayId);
        Task<IEnumerable<Item>> GetAllItemsForOrderIdAsync(int orderId);
    }
}
