﻿using HolidayOrders.Xamarin.Models;
using HolidayOrders.Xamarin.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using PCLAppConfig;

namespace HolidayOrders.Xamarin.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        private ObservableCollection<Holiday> _allHolidays;
        private ObservableCollection<Order> _allOrders;
        private ObservableCollection<Order> _filteredOrders;

        private Holiday _selectedHoliday;
        private Order _selectedOrder;
        private bool _isLoadingOrders;
        private string _searchText;

        private IDataProvider _dataProvider;
        private ICommand m_refreshDataCommand;
        private ICommand m_navigateToOrderPageCommand;
        private ICommand m_filterOrdersCommand;

        public ObservableCollection<Holiday> AllHolidays
        {
            get => _allHolidays;
            set => SetProperty(ref _allHolidays, value);
        }

        public bool IsLoadingOrders { get => _isLoadingOrders; set => SetProperty(ref _isLoadingOrders, value); }
        public string SearchText
        {
            get => _searchText;
            set { SetProperty(ref _searchText, value); }
        }

        public ObservableCollection<Order> AllOrders { get => _allOrders; set => SetProperty(ref _allOrders, value); }
        public ObservableCollection<Order> FilteredOrders { get => _filteredOrders; set => SetProperty(ref _filteredOrders, value); }

        public Holiday SelectedHoliday
        {
            get => _selectedHoliday;
            set
            {
                SetProperty(ref _selectedHoliday, value);
                RefreshOrders();
            }
        }

        public Order SelectedOrder
        {
            get => _selectedOrder; set => SetProperty(ref _selectedOrder, value);
        }

        public ICommand RefreshDataCommand { get => m_refreshDataCommand; set => SetProperty(ref m_refreshDataCommand, value); }

        public ICommand NavigateToOrderPageCommand { get => m_navigateToOrderPageCommand; set => SetProperty(ref m_navigateToOrderPageCommand, value); }
        public ICommand FilterOrdersCommand { get => m_filterOrdersCommand; set => SetProperty(ref m_filterOrdersCommand, value); }

        public MainPageViewModel(INavigationService navigationService, IDataProvider dataProvider)
            : base(navigationService)
        {
            Title = "דרך האוכל - צפיה בהזמנות לחגים";
            _dataProvider = dataProvider;
            IsLoadingOrders = false;
            m_refreshDataCommand = new DelegateCommand(RefreshData);
            m_navigateToOrderPageCommand = new DelegateCommand(NevigateToOrderPage);
            m_filterOrdersCommand = new DelegateCommand(FilterOrders);
            SearchText = string.Empty;
        }

        public override async void OnNavigatedTo(NavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            if (AllHolidays == null)
            {
                RefreshData();
            }
        }


        public void FilterOrders()
        {
            FilteredOrders.Clear();
            AllOrders.Where(t => t.Name.ToLower().Contains(SearchText.ToLower())).ToList().ForEach(t => FilteredOrders.Add(t));
        }
        private async void NevigateToOrderPage()
        {
            var parameters = new NavigationParameters();
            parameters.Add("Order", SelectedOrder);
            await NavigationService.NavigateAsync("NavigationPage/OrderPage", parameters);
        }

        public async void RefreshData()
        {
            IsLoadingOrders = true;
            var allHolidays = await _dataProvider.GetAllHolidaysAsync();
            if (allHolidays != null)
            {
                AllHolidays = new ObservableCollection<Holiday>(allHolidays);
                if (SelectedHoliday == null)
                {
                    SelectHoliday();
                }
            }

            IsLoadingOrders = false;

        }

        private void SelectHoliday()
        {
            //try
            //{
            //var selectedHolidayFromConfig = ConfigurationManager.AppSettings["SelectedHolidayId"];
            //if (selectedHolidayFromConfig != null)
            //{
            //var holiday = AllHolidays.FirstOrDefault(x => x.Id == selectedHolidayFromConfig);
            //SelectedHoliday = holiday;
            //    }
            //    else
            //    {
            SelectedHoliday = AllHolidays.FirstOrDefault();
            //    }

            //}
            //catch (Exception e)
            //{
            //    SelectedHoliday = AllHolidays.FirstOrDefault();
            //}


        }

        public async void RefreshOrders()
        {
            IsLoadingOrders = true;
            if (SelectedHoliday != null && SelectedHoliday.Id != null)
            {
                AllOrders?.Clear();
                FilteredOrders?.Clear();
                var allOrders = await _dataProvider.GetOrdersForHolidayIdAsync(int.Parse(SelectedHoliday.Id));
                if (allOrders != null)
                {
                    AllOrders = new ObservableCollection<Order>(allOrders);
                    FilteredOrders = new ObservableCollection<Order>(AllOrders);
                    SearchText = string.Empty;
                }
            }
            IsLoadingOrders = false;
            StoreSelectedHolidayId();
        }

        private void StoreSelectedHolidayId()
        {

        }
    }
}
