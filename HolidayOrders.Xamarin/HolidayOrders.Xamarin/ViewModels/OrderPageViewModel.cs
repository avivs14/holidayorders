﻿using HolidayOrders.Xamarin.Models;
using HolidayOrders.Xamarin.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace HolidayOrders.Xamarin.ViewModels
{
    public class OrderPageViewModel : ViewModelBase 
    {
        private IDataProvider _dataProvider;
        private ICommand m_refreshDataCommand;
        private ICommand m_makeACallCommand;

        private ObservableCollection<Item> _items;
        private Order _order;
        private bool m_isLoadingItems;

        public Order Order
        {
            get => _order;
            set => SetProperty(ref _order, value);
        }
        public ObservableCollection<Item> Items
        {
            get => _items;
            set => SetProperty(ref _items, value);
        }

        public bool IsLoadingItems { get => m_isLoadingItems; set => SetProperty(ref m_isLoadingItems, value); }

        public OrderPageViewModel(INavigationService navigationService, IDataProvider dataProvider)
            : base(navigationService)
        {
            _dataProvider = dataProvider;
            RefreshDataCommand = new DelegateCommand(RefreshData);
            MakeACallCommand = new DelegateCommand(MakeACall);
            IsLoadingItems = false;
        }

        private void MakeACall()
        {
            Device.OpenUri(new Uri($"tel:{Order.Phone}"));
        }

        public override void OnNavigatedTo(NavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            if (parameters.ContainsKey("Order"))
            {
                Order = parameters["Order"] as Order;
                if (Order != null)
                {
                    Title = $"הזמנה {Order.OrderNumber} של {Order.Name}";
                    RefreshData();
                }
            }
            
        }
        public ICommand RefreshDataCommand { get => m_refreshDataCommand; set => SetProperty(ref m_refreshDataCommand, value); }
        public ICommand MakeACallCommand { get => m_makeACallCommand; set => SetProperty(ref m_makeACallCommand, value); }

        private async void RefreshData()
        {
            IsLoadingItems = true;
            Items?.Clear();
        
            var items =await _dataProvider.GetAllItemsForOrderIdAsync(int.Parse(_order.OrderId));
            if (items != null)
            {
                Items = new ObservableCollection<Item>(items);
            }

            IsLoadingItems = false;
        }
    }
}
