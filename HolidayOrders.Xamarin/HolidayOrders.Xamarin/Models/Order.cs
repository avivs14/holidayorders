﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HolidayOrders.Xamarin.Models
{
    public class Order
    {
        public string OrderId { get; set; }
        public string Phone { get; set; }
        public string Comments { get; set; }
        public string Name { get; set; }
        public string Holiday { get; set; }
        public string HolidayID { get; set; }
        public string OrderNumber { get; set; }
    }
}
