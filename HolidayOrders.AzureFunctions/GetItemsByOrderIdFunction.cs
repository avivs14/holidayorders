
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.Http;
using HolidayOrders.AzureFunctions.Dal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;

namespace HolidayOrders.AzureFunctions
{
    public static class GetItemsByOrderIdFunction
    {
        [FunctionName("GetItemsByOrderIdFunction")]
        public static IActionResult Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]HttpRequest req, TraceWriter log)
        {
            var parameters = req.GetQueryParameterDictionary();
            if (parameters.ContainsKey("OrderId"))
            {
                var OrderId = parameters["OrderId"];
                var results = DbHelper.GetItemsByOrderId(OrderId);
                if (results != null)
                {
                    return new OkObjectResult(results);
                }
            }
            return new InternalServerErrorResult();
        }
    }
}
