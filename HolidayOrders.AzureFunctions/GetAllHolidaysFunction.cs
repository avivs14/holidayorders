
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.Http;
using HolidayOrders.AzureFunctions.Dal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;

namespace HolidayOrders.AzureFunctions
{
    public static class GetAllHolidaysFunction
    {
        [FunctionName("GetAllHolidaysFunction")]
        public static IActionResult Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]HttpRequest req, TraceWriter log)
        {
            var results = DbHelper.GetAllHolidays(); ;
            if (results != null)
            {
                return new OkObjectResult(results);
            }
            return new InternalServerErrorResult();
        }
    }
}
