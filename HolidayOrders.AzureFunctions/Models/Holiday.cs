﻿namespace HolidayOrders.AzureFunctions.Models
{
    public class Holiday
    {
        public string Name { get; set; }
        public string Id { get; set; }
    }
}