﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using HolidayOrders.AzureFunctions.Models;
using Microsoft.AspNetCore.Mvc;

namespace HolidayOrders.AzureFunctions.Dal
{
    public static class DbHelper
    {
        public static IEnumerable<T> ExecuteSp<T>(string spName, Func<IDataReader, IEnumerable<T>> transformFunc)
        {
            using (SqlConnection conn = new SqlConnection(DalConsts.ConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand() { CommandType = CommandType.StoredProcedure, CommandText = spName, Connection = conn })
                {
                    var reader = cmd.ExecuteReader();
                    return transformFunc(reader);
                }

            }
        }

        public static IEnumerable<T> ExecuteQuery<T>(string queryText, Func<IDataReader, IEnumerable<T>> transformFunc)
        {
            using (SqlConnection conn = new SqlConnection(DalConsts.ConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand() { CommandText = queryText, Connection = conn })
                {
                    var dataReader = cmd.ExecuteReader();
                    return transformFunc(dataReader);
                }
            }
        }

        public static IEnumerable<Order> GetAllOrders()
        {
            return ExecuteSp(DalConsts.GetAllOrdersSPName, reader => reader.ToOrders());
        }

        public static IEnumerable<Order> GetAllOrdersForHolidayId(string holidayId)
        {
            return ExecuteQuery(DalConsts.GetAllByHolidayIdQueryText(holidayId), reader => reader.ToOrders());
        }

        public static IEnumerable<Item> GetItemsByOrderId(string orderId)
        {
            return ExecuteQuery(DalConsts.GetItemsByOrderIdQueryText(orderId), reader => reader.ToItems());
        }

        public static IEnumerable<Holiday> GetAllHolidays()
        {
            return ExecuteQuery(DalConsts.GetAllHolidaysQueryText, reader => reader.ToHolidays());
        }
    }
}
