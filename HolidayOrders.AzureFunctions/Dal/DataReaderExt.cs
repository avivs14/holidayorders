﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using HolidayOrders.AzureFunctions.Models;

namespace HolidayOrders.AzureFunctions.Dal
{
    public static class DataReaderExt
    {
        public static List<Order> ToOrders(this IDataReader reader)
        {
            var results = new List<Order>();
            var table = new DataTable();
            table.Load(reader);
            reader.Close();

            foreach (DataRow tableRow in table.Rows)
            {
                results.Add(new Order()
                {
                    OrderId = tableRow["OrderId"].ToString(),
                    Comments = tableRow["Comments"].ToString(),
                    Phone = tableRow["Phone"].ToString(),
                    Name = tableRow["Name"].ToString(),
                    Holiday = tableRow["Holiday"].ToString(),
                    HolidayID = tableRow["HolidayID"].ToString(),
                    OrderNumber = tableRow["OrderNumber"].ToString()
                });
            }

            return results;

        }

        public static List<Holiday> ToHolidays(this IDataReader reader)
        {
            var results = new List<Holiday>();
            var table = new DataTable();
            table.Load(reader);
            reader.Close();

            foreach (DataRow tableRow in table.Rows)
            {
                results.Add(new Holiday() { Id = tableRow["Id"].ToString(), Name = tableRow["Name"].ToString() });
            }

            return results;

        }

        public static List<Item> ToItems(this IDataReader reader)
        {
            var results = new List<Item>();
            var table = new DataTable();
            table.Load(reader);
            reader.Close();

            foreach (DataRow tableRow in table.Rows)
            {
                var toReturn = new Item() { Food = tableRow["FoodName"].ToString() };
                if (bool.Parse(tableRow["IsUnits"].ToString()))//isUnits
                {
                    toReturn.Ammount = tableRow["Units"].ToString();
                    toReturn.Units = "יחידות";
                }
                else
                {
                    var isBox0f = bool.Parse(tableRow["IsBoxOf"].ToString());
                    var boxOf = isBox0f ? "קופסא של " : string.Empty;

                    toReturn.Ammount = boxOf + tableRow["DisplayWeight"].ToString();
                    toReturn.Units = tableRow["DisplayUnits"].ToString();
                }
                results.Add(toReturn);
            }

            return results;

        }
    }
}
