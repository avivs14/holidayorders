﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HolidayOrders.AzureFunctions.Dal
{
    public static class DalConsts
    {
        public static string ConnectionString =
            "Server=tcp:holidaydb.database.windows.net,1433;Initial Catalog=HolidayDB;Persist Security Info=False;User ID=avivs14;Password=avivs1994!;MultipleActiveResultSets=False;Encrypt=False;Trusted_Connection=False;TrustServerCertificate=False;Connection Timeout=30;";
        public static string GetAllOrdersSPName = "[dbo].[GetAllOrders]";

        public static string GetAllHolidaysQueryText = @"SELECT [ID],[Name]
                                                        FROM [dbo].[HolidaySet]";

        public static string GetAllByHolidayIdQueryText(string holidayId)
        {
            return $@"SELECT [dbo].[Orders].[Id] as OrderId
	                          ,[Phone] as Phone
	                          ,[Comments] as Comments
                              ,[CustomerName] as Name
	                          ,[dbo].[HolidaySet].[Name] as Holiday
                              ,[HolidayId] as [HolidayId]
                              ,concat([HolidaySet].[Symbol],'',[OrderNumber]) as OrderNumber
     
                              FROM [dbo].[Orders]
                              JOIN [dbo].[HolidaySet]
                              ON [dbo].[Orders].[HolidayId] = [dbo].[HolidaySet].[Id]
                             WHERE [HolidaySet].[Id] = {holidayId}";

        }


        public static string GetItemsByOrderIdQueryText(string orderId)
        {
            return $@"SELECT [IsUnits]
                              ,[Units]
                              ,[dbo].[BoxSet].[DisplayWeight] as DisplayWeight
	                          ,[dbo].[BoxSet].[DisplayUnits] as DisplayUnits
	                          ,[dbo].[Foods].[Name] as FoodName
                              ,[IsBoxOf]
                              FROM [dbo].[Items] 
                              JOIN [dbo].[Foods]
                              ON [dbo].[Foods].[Id] = [dbo].[Items].[FoodId]
                              JOIN [dbo].[BoxSet] 
                              ON [dbo].[BoxSet].[Id] = [dbo].[Items].[BoxId]
                              WHERE [OrderId]={orderId}";

        }
    }
}
