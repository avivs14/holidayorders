﻿
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 04/04/2016 21:57:39
-- Generated from EDMX file: C:\Users\Aviv\Documents\HolidayOrders.UI\HolidayOrders.UI\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_HolidayOrder]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Orders] DROP CONSTRAINT [FK_HolidayOrder];
GO
IF OBJECT_ID(N'[dbo].[FK_OrderItem]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Items] DROP CONSTRAINT [FK_OrderItem];
GO
IF OBJECT_ID(N'[dbo].[FK_BoxItem]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Items] DROP CONSTRAINT [FK_BoxItem];
GO
IF OBJECT_ID(N'[dbo].[FK_FoodItem]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Items] DROP CONSTRAINT [FK_FoodItem];
GO
IF OBJECT_ID(N'[dbo].[FK_HolidayFood]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Foods] DROP CONSTRAINT [FK_HolidayFood];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[HolidaySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[HolidaySet];
GO
IF OBJECT_ID(N'[dbo].[Orders]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Orders];
GO
IF OBJECT_ID(N'[dbo].[Items]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Items];
GO
IF OBJECT_ID(N'[dbo].[Foods]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Foods];
GO
IF OBJECT_ID(N'[dbo].[BoxSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[BoxSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'HolidaySet'
CREATE TABLE [dbo].[HolidaySet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Orders'
CREATE TABLE [dbo].[Orders] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [CustomerName] nvarchar(max)  NOT NULL,
    [HolidayId] int  NOT NULL,
    [OrderNumber] int  NOT NULL,
    [Comments] nvarchar(max)  NOT NULL,
    [Phone] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Items'
CREATE TABLE [dbo].[Items] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [OrderId] int  NOT NULL,
    [IsUnits] bit  NOT NULL,
    [Units] int  NOT NULL,
    [BoxId] int  NOT NULL,
    [FoodId] int  NOT NULL
);
GO

-- Creating table 'Foods'
CREATE TABLE [dbo].[Foods] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [HolidayId] int  NOT NULL
);
GO

-- Creating table 'BoxSet'
CREATE TABLE [dbo].[BoxSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Weight] float  NOT NULL,
    [DisplayUnits] nvarchar(max)  NOT NULL,
    [DisplayWeight] nvarchar(max)  NOT NULL,
    [DisplayName] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'HolidaySet'
ALTER TABLE [dbo].[HolidaySet]
ADD CONSTRAINT [PK_HolidaySet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [PK_Orders]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Items'
ALTER TABLE [dbo].[Items]
ADD CONSTRAINT [PK_Items]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Foods'
ALTER TABLE [dbo].[Foods]
ADD CONSTRAINT [PK_Foods]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'BoxSet'
ALTER TABLE [dbo].[BoxSet]
ADD CONSTRAINT [PK_BoxSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [HolidayId] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [FK_HolidayOrder]
    FOREIGN KEY ([HolidayId])
    REFERENCES [dbo].[HolidaySet]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_HolidayOrder'
CREATE INDEX [IX_FK_HolidayOrder]
ON [dbo].[Orders]
    ([HolidayId]);
GO

-- Creating foreign key on [OrderId] in table 'Items'
ALTER TABLE [dbo].[Items]
ADD CONSTRAINT [FK_OrderItem]
    FOREIGN KEY ([OrderId])
    REFERENCES [dbo].[Orders]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_OrderItem'
CREATE INDEX [IX_FK_OrderItem]
ON [dbo].[Items]
    ([OrderId]);
GO

-- Creating foreign key on [BoxId] in table 'Items'
ALTER TABLE [dbo].[Items]
ADD CONSTRAINT [FK_BoxItem]
    FOREIGN KEY ([BoxId])
    REFERENCES [dbo].[BoxSet]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_BoxItem'
CREATE INDEX [IX_FK_BoxItem]
ON [dbo].[Items]
    ([BoxId]);
GO

-- Creating foreign key on [FoodId] in table 'Items'
ALTER TABLE [dbo].[Items]
ADD CONSTRAINT [FK_FoodItem]
    FOREIGN KEY ([FoodId])
    REFERENCES [dbo].[Foods]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FoodItem'
CREATE INDEX [IX_FK_FoodItem]
ON [dbo].[Items]
    ([FoodId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------