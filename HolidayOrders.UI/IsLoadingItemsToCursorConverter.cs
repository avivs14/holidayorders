﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Input;

namespace HolidayOrders.UI
{
    public class IsLoadingItemsToCursorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool isLoading = (bool) value;
            if (isLoading)
            {
                return Cursors.Wait;
            }
            return Cursors.Arrow;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}