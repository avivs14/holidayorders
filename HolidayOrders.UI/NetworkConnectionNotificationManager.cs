﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.NetworkInformation;
using System.Reactive.Linq;

namespace HolidayOrders.UI
{
    public class NetworkConnectionNotificationManager
    {

        public IObservable<bool> IsActive
        {
            get
            {
                return Observable.FromEventPattern<NetworkAvailabilityChangedEventHandler, NetworkAvailabilityEventArgs>(
                                      ev => NetworkChange.NetworkAvailabilityChanged += ev,
                                      ev => NetworkChange.NetworkAvailabilityChanged -= ev)
                                      .Select(ev => ev.EventArgs.IsAvailable);
            }

        }
    }
}
