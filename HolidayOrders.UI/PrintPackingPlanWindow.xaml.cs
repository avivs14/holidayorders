﻿/************************************************************************
 * Copyright: Hans Wolff
 *
 * License:  This software abides by the LGPL license terms. For further
 *           licensing information please see the top level LICENSE.txt 
 *           file found in the root directory of CodeReason Reports.
 *
 * Author:   Hans Wolff
 *
 ************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.IO.Packaging;
using System.Threading.Tasks;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Xps.Packaging;
using CodeReason.Reports;
using MahApps.Metro.Controls;

namespace HolidayOrders.UI
{
    /// <summary>
    /// Application's main form
    /// </summary>
    public partial class PrintPackingPlanWindow : MetroWindow
    {
        internal static Settings Config = Settings.Default;

        public string FilePath { get; set; }
        private bool _firstActivated = true;
        private IList<PackingPage> _packingPages;
        /// <summary>
        /// Constructor
        /// </summary>
        public PrintPackingPlanWindow(IList<PackingPage> packingPages)
        {
            _packingPages = packingPages;
            InitializeComponent();
        }

        /// <summary>
        /// Window has been activated
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event details</param>
        private void RemoveOldFiles()
        {
            var v = Directory.GetFiles("".AddCurrentDirectory());
            v.Where(x => x.Contains("tmp") || x.Contains("סיכום")).ToList().ForEach(x =>
            {
                try
                {
                    File.Delete(x);
                }
                catch (Exception)
                {
                }
            });
        }
        public async Task ActivateWindow()
        {
            if (!_firstActivated) return;

            _firstActivated = false;


            await Task.Run(async () =>
            {
                try
                {
                    RemoveOldFiles();

                    var allXps = new List<XpsDocument>();
                    if (_packingPages.Any())
                    {
                        foreach (var packingPage in _packingPages)
                        {
                            var xps = CreateXps(packingPage);
                            allXps.Add(xps);
                        }


                        FilePath = Path.ChangeExtension("סיכום" + Path.GetFileName(Path.GetTempFileName()), ".xps").AddCurrentDirectory();


                        await Application.Current.Dispatcher.InvokeAsync(() =>
                        {
                            allXps.MergeXpsDocumentAsync(FilePath);
                            var mergedXpsDocument = new XpsDocument(FilePath, FileAccess.ReadWrite);
                            documentViewer.Document = mergedXpsDocument.GetFixedDocumentSequence();

                            mergedXpsDocument.CleanPackageFromMemory();
                    
                        });
                    }
                }
                catch (Exception ex)
                {
                    // show exception
                    MessageBox.Show("ארעה תקלה בעת הכנת התצוגה המקדימה להדפסה", "אופס",
                        MessageBoxButton.OK, MessageBoxImage.Stop);
                }


                try
                {
                }
                catch (Exception ex)
                {
                    // show exception
                    MessageBox.Show(ex.Message + "\r\n\r\n" + ex.GetType() + "\r\n" + ex.StackTrace, ex.GetType().ToString(), MessageBoxButton.OK, MessageBoxImage.Stop);
                }
            });
        }

        private static XpsDocument CreateXps(PackingPage packingPage)
        {
            ReportDocument reportDocument = new ReportDocument();

            StreamReader reader = new StreamReader(new FileStream(Config.PackingPlanReportTemplatePath, FileMode.Open, FileAccess.Read));
            reportDocument.XamlData = reader.ReadToEnd();
            reportDocument.XamlImagePath = Path.Combine(Environment.CurrentDirectory, Config.TemplateBasePath);
            reader.Close();

            ReportData data = new ReportData();

            // set constant document values
            data.ReportDocumentValues.Add("PrintDate", DateTime.Now); // print date is now
            data.ReportDocumentValues.Add("ReportTitle", "תוכנית אריזה - " + packingPage.FoodName + " - " + packingPage.HolidayName);
            // sample table "Ean"
            DataTable table = new DataTable("Order");

            table.Columns.Add("Units", typeof(string));
            table.Columns.Add("Ammount", typeof(string));
            table.Columns.Add("HowManyBoxes", typeof(string));

            foreach (var item in packingPage.PackingRows)
            {

                table.Rows.Add(item.BoxWeightOrUnits, item.Ammount, item.HowManyBoxes);

            }
            Random rnd = new Random(1234);

            data.DataTables.Add(table);

            var fileName =
                Path.ChangeExtension(Path.Combine(Path.GetFileName(Path.GetTempFileName()).AddCurrentDirectory()),
                    ".xps");
            var xps = reportDocument.CreateXpsDocument(data, fileName);
            var xpsPath = fileName;

            return xps;
        }
           
    }
}
