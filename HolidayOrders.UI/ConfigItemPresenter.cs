﻿namespace HolidayOrders.UI
{
    public class ConfigItemPresenter
    {
        public string Translation { get; set; }
        public string ConfigKey { get; set; }

        public object Value
        {
            get { return _value; }
            set
            {
                _value = value;
                _config[ConfigKey] = _value;
                _config.Save();
            }
        }

        private Settings _config;
        private object _value;

        public ConfigItemPresenter(string configKey,string translation)
        {
            _config = Settings.Default;
            ConfigKey = configKey;
            Translation = translation;
            Value= _config[configKey];
        }
    }
}
