﻿using System.Collections.Generic;

namespace HolidayOrders.UI
{
    public class StickersLine
    {
        public List<StickerCell> Line { get; set; }

        public StickersLine()
        {
            Line = new List<StickerCell>();
        }
    }
}
