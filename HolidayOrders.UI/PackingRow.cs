﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HolidayOrders.UI
{
    public class PackingRow
    {
        public int HowManyBoxes  { get; set; }
        public string Ammount { get; set; }
        public string BoxWeightOrUnits { get; set; }

        public PackingRow(int howManyBoxes, string ammount, string boxWeightOrUnits)
        {
            HowManyBoxes = howManyBoxes;
            Ammount = ammount;
            BoxWeightOrUnits = boxWeightOrUnits;
        }
    }
}
