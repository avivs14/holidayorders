﻿namespace HolidayOrders.UI
{
    public class StickerCell
    {
        private const int CharsInLine = 25;
        private string _line1;
        private string _line2;

        public string Line1
        {
            get { return _line1; }
            set
            {
                _line1 = value.Trim();
                if (_line1.Length > CharsInLine)
                {
                    _line1 = _line1.Substring(0, CharsInLine);
                }
            }
        }

        public string Line2
        {
            get { return _line2; }
            set
            {
                _line2 = value.Trim();
                if (_line2.Length > CharsInLine)
                {
                    _line2 = _line2.Substring(0, CharsInLine);
                }
            }
        }

        public string Line3 { get; set; }
        public string Line4 { get; set; }
        public string Line5 { get; set; }

    }
}
