﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using MonthSummery.UI;
using Prism.Commands;

namespace HolidayOrders.UI
{
    public class ChooseHolidayPresenter : INotifyPropertyChanged
    {
        private Holiday _selectedHoliday;
        private ObservableCollection<Holiday> _possibleHolidays;
        public Holiday SelectedHoliday
        {
            get { return _selectedHoliday; }
            set
            {
                _selectedHoliday = value;
                NotifyPropertyChanged("SelectedHoliday");
            }
        }

        public DelegateCommand RenameCommand { get; set; }
        public ObservableCollection<Holiday> PossibleHolidays
        {
            get { return _possibleHolidays; }
            set
            {
                _possibleHolidays = value;
                NotifyPropertyChanged("PossibleHolidays");
            }
        }

        public DelegateCommand DeleteHolidayCommand { get; set; }
        public DelegateCommand CreateNewHolidayCommand { get; set; }
        public DelegateCommand OpenHolidayCommand { get; set; }
        public ChooseHolidayPresenter()
        {
            BackupManager.BackupOnline();
            InitializePossibleHolidays();
            CreateNewHolidayCommand = new DelegateCommand(CreateNewHoliday);
            OpenHolidayCommand = new DelegateCommand(OpenHoliday);
            RenameCommand = new DelegateCommand(RenameHoliday);
            DeleteHolidayCommand = new DelegateCommand(DeleteCommand);
        }

        private async void DeleteCommand()
        {

            var metroWindow = Application.Current.Windows.Cast<Window>().First(x => x is ChooseHolidayWindow) as MetroWindow;
            var settings = new MetroDialogSettings();
            settings.NegativeButtonText = "כן, אני בטוח";
            settings.AffirmativeButtonText = "ביטול";
            var result = await metroWindow.ShowMessageAsync("האם אתה בטוח?",
                "מחיקה של החג תמחק את כל ההזמנות וכל הפריטים שבתוכו!, האם אתה בטוח שברצונך למחוק חג זה ?",
                MessageDialogStyle.AffirmativeAndNegative, settings);

            if (result == MessageDialogResult.Negative)
            {
                int selectedIndex = PossibleHolidays.IndexOf(SelectedHoliday);

                BackupManager.BackupLocal();

                var db = DB.Instance;
                var holiday = db.HolidaySet.SingleOrDefault(x => x.Id == SelectedHoliday.Id);
                db.HolidaySet.Remove(holiday);
                PossibleHolidays.Remove(SelectedHoliday);
                if (PossibleHolidays.Any())
                {
                    if (selectedIndex > 0)
                    {
                        selectedIndex--;
                    }
                    SelectedHoliday = PossibleHolidays[selectedIndex];
                }
                DB.Save(db);
            }
        }

        private void InitializePossibleHolidays()
        {
            var db = DB.Instance;
            PossibleHolidays = new ObservableCollection<Holiday>(DB.Instance.HolidaySet.ToList());
            if (PossibleHolidays.Any())
            {
                SelectedHoliday = PossibleHolidays.First();
            }
        }

        private void RenameHoliday()
        {
            //EnterStringWindow enterStringWindow = new EnterStringWindow("שינוי שם חג", "לאיזה חג תרצה לשנות ?",SelectedHoliday.Name);
            //enterStringWindow.ShowDialog();

            var db = DB.Instance;
            var holiday = db.HolidaySet.SingleOrDefault(x => x.Id == SelectedHoliday.Id);
            if (holiday != null)
            {
                if (holiday.Name != SelectedHoliday.Name)
                {
                    holiday.Name = SelectedHoliday.Name;
                }
                if (holiday.Symbol != SelectedHoliday.Symbol)
                {
                    holiday.Symbol = SelectedHoliday.Symbol;
                }
            }

            DB.Save(db);
        }


        private void OpenHoliday()
        {
            MainWindow window = new MainWindow();
            window.Show();
            var mainPresenter = window.DataContext as MainPresenter;
            if (mainPresenter != null)
            {
                mainPresenter.SelectedHoliday = SelectedHoliday;
            }
            var metroWindow = Application.Current.Windows.Cast<Window>().First(x => x is ChooseHolidayWindow) as MetroWindow;
            metroWindow.Close();

        }

        private async void CreateNewHoliday()
        {
            var metroWindow = Application.Current.Windows.Cast<Window>().First(x => x is ChooseHolidayWindow) as MetroWindow;

            var settings = new MetroDialogSettings();
            settings.AffirmativeButtonText = "אישור";
            settings.NegativeButtonText = "ביטול";
            settings.DefaultText = "בחר שם לחג";

            var result = await metroWindow.ShowInputAsync("הוספת חג חדש", "איזה חג תרצה להוסיף?", settings);
            if (result != null)
            {
                var db = DB.Instance;
                db.HolidaySet.Add(new Holiday() { Name = result,Symbol = "א"});
                DB.Save(db);
                PossibleHolidays = new ObservableCollection<Holiday>(DB.Instance.HolidaySet.ToList());
                SelectedHoliday = PossibleHolidays.Last();
            }

        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string info)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(info));
        }
    }
}
