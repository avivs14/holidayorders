﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;

namespace HolidayOrders.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {

        private int _lastKey;
        public MainWindow()
        {
            InitializeComponent();

        }

        public void OrderAdded()
        {
            var border = VisualTreeHelper.GetChild(OrdersDataGrid, 0) as Decorator;
            if (border != null)
            {
                var scroll = border.Child as ScrollViewer;
                if (scroll != null) scroll.ScrollToEnd();
            }
            OrdersDataGrid.SelectedIndex = OrdersDataGrid.Items.Count - 1;

           var row  = OrdersDataGrid.GetRowByIndex(OrdersDataGrid.Items.Count - 1);
            row.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
            
        }
        private void UIElement_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            
            if (!Keyboard.IsKeyDown(Key.Tab)) return;
            if (ItemsDataGrid.SelectedItem is ItemWrapper)
            {
                var item = ItemsDataGrid.SelectedItem as ItemWrapper;
                if (ItemsDataGrid.SelectedIndex == ItemsDataGrid.Items.Count - 1)
                {
                    if (item.Item.IsUnits && sender is NumericUpDown || !item.Item.IsUnits && sender is CheckBox)
                    {
                        if (ItemsDataGrid.DataContext is MainPresenter)
                        {
                            (ItemsDataGrid.DataContext as MainPresenter).AddItemCommand.Execute();
                            if (ItemsDataGrid.Items.Count > 0)
                            {
                                var border = VisualTreeHelper.GetChild(ItemsDataGrid, 0) as Decorator;
                                if (border != null)
                                {
                                    var scroll = border.Child as ScrollViewer;
                                    if (scroll != null) scroll.ScrollToEnd();
                                }
                                ItemsDataGrid.SelectedItem = ItemsDataGrid.Items[ItemsDataGrid.Items.Count - 1];
                            }
                        }
                    }
                }
            }

        }

        private void ChangeHolidayClicked(object sender, RoutedEventArgs e)
        {
            var flyout = this.Flyouts.Items[0] as Flyout;
            if (flyout is CustomFlyout)
            {
                (flyout as CustomFlyout).Holidays = new ObservableCollection<Holiday>(DB.Instance.HolidaySet.ToList());
            }
            flyout.IsOpen = true;
        }

        private void Control_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var flyout = this.Flyouts.Items[0] as Flyout;
            flyout.IsOpen = false;
        }

        private void SelctionChangedSwitchHoliday(object sender, SelectionChangedEventArgs e)
        {
            var flyout = this.Flyouts.Items[0] as Flyout;
            flyout.IsOpen = false;
        }

        private void SelctionChangedMoveHoliday(object sender, SelectionChangedEventArgs e)
        {
            var flyout = this.Flyouts.Items[2] as Flyout;
            flyout.IsOpen = false;
        }

        private void OpenConfigClicked(object sender, RoutedEventArgs e)
        {
            var flyout = this.Flyouts.Items[1] as Flyout;
            flyout.IsOpen = true;
        }

        private void MenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            var flyout = this.Flyouts.Items[2] as Flyout;
            flyout.IsOpen = true;
        }
    }
}
