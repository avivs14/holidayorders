﻿using System;
using System.Windows;

namespace HolidayOrders.UI
{
    public static class DB
    {
        public static HolidayDBEntities Instance => new HolidayDBEntities();

        public static void Save(HolidayDBEntities db)
        {
            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ארעה תקלה בשמירת המידע", ex.Message, MessageBoxButton.OK);
            }
        }

        public static HolidayDBEntities GetInstanceForPath(string dbFilePath)
        {
            var instance = new HolidayDBEntities();
            instance.Database.Connection.ConnectionString = @"Data Source = " + dbFilePath;
            return instance;
        }

    }
}