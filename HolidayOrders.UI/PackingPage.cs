﻿using System.Collections.Generic;

namespace HolidayOrders.UI
{
    public class PackingPage
    {

        public PackingPage(string holidayName, string foodName, List<PackingRow> packingRows)
        {
            HolidayName = holidayName;
            FoodName = foodName;
            PackingRows = packingRows;
        }

        public string HolidayName { get; set; } 
        public string FoodName { get; set; }
       public List<PackingRow> PackingRows { get; set; }  
    }
}