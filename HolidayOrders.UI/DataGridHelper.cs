using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace HolidayOrders.UI
{
    public static class DataGridHelper
    {
        public static DataGridColumn GetColumnByIndices(this DataGrid dataGrid, int rowIndex, int columnIndex)
        {
            if (dataGrid == null)
                return null;

            //Validate Indices
            ValidateColumnIndex(dataGrid, columnIndex);
            ValidateRowIndex(dataGrid, rowIndex);


            var row = dataGrid.GetRowByIndex(rowIndex);


            if (row != null)//Get Column for the DataGridRow by index using GetRowColumnByIndex Extension methods
                return row.GetRowColumnByIndex(dataGrid, columnIndex);

            return null;
        }

        public static DataGridCell GetCellByIndices(this DataGrid dataGrid, int rowIndex, int columnIndex)
        {
            if (dataGrid == null)
                return null;

            //Validate Indices
            ValidateColumnIndex(dataGrid, columnIndex);
            ValidateRowIndex(dataGrid, rowIndex);

            var row = dataGrid.GetRowByIndex(rowIndex);

            if (row != null)
                return row.GetCellByColumnIndex(dataGrid, columnIndex);

            return null;
        }

        public static DataGridRow GetRowByIndex(this DataGrid dataGrid, int rowIndex)
        {
            if (dataGrid == null)
                return null;

            ValidateRowIndex(dataGrid, rowIndex);

            dataGrid.UpdateLayout();
            dataGrid.ScrollIntoView(dataGrid.Items[rowIndex]);

            return (DataGridRow)dataGrid.ItemContainerGenerator.ContainerFromIndex(rowIndex);
        }

        public static DataGridColumn GetRowColumnByIndex(this DataGridRow row, DataGrid dataGrid, int columnIndex)
        {

            if (row == null || dataGrid == null)
                return null;

            ValidateColumnIndex(dataGrid, columnIndex);

            var cell = GetCellByColumnIndex(row, dataGrid, columnIndex);

            if (cell != null)
                return cell.Column;

            return null;
        }

        public static DataGridCell GetCellByColumnIndex(this DataGridRow row, DataGrid dataGrid, int columnIndex)
        {
            if (row == null || dataGrid == null)
                return null;

            ValidateColumnIndex(dataGrid, columnIndex);

            DataGridCellsPresenter cellPresenter = row.FindVisualChild<DataGridCellsPresenter>();

            if (cellPresenter != null)
                return ((DataGridCell)cellPresenter.ItemContainerGenerator.ContainerFromIndex(columnIndex));

            return null;
        }

        public static T GetUIElementOfCell<T>(this DataGrid dataGrid, int rowIndex, int columnIndex) where T : Visual
        {
            var cell = GetCellByIndices(dataGrid, rowIndex, columnIndex);

            if (cell != null)
                return cell.FindVisualChild<T>();

            return null;
        }

        public static IEnumerable<T> GetUIElementsOfCell<T>(this DataGrid dataGrid, int rowIndex, int columnIndex) where T : Visual
        {
            var cell = GetCellByIndices(dataGrid, rowIndex, columnIndex);

            if (cell != null)
                return cell.FindVisualChilds<T>();

            return null;
        }

        public static T GetUIElementOfCell<T>(this DataGridCell dataGridCell) where T : Visual
        {
            if (dataGridCell == null)
                return null;

            return dataGridCell.FindVisualChild<T>();
        }

        private static void ValidateColumnIndex(DataGrid dataGrid, int columnIndex)
        {
            if (columnIndex >= dataGrid.Columns.Count)
                throw new IndexOutOfRangeException("columnIndex out of range");
        }

        private static void ValidateRowIndex(DataGrid dataGrid, int rowIndex)
        {
            if (rowIndex >= dataGrid.Items.Count)
                throw new IndexOutOfRangeException("rowIndex out of range");
        }
    }
}