//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HolidayOrders.UI
{
    using System;
    using System.Collections.Generic;
    
    public partial class Item
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public bool IsUnits { get; set; }
        public int Units { get; set; }
        public int BoxId { get; set; }
        public int FoodId { get; set; }
        public bool IsBoxOf { get; set; }
    
        public virtual Order Order { get; set; }
        public virtual Box Box { get; set; }
        public virtual Food Food { get; set; }
    }
}
