﻿using System.Collections.ObjectModel;

namespace HolidayOrders.UI
{
    public class ConfigPresenter
    {
        public ObservableCollection<ConfigItemPresenter> allConfigs { get; set; }

        public ConfigPresenter()
        {
            allConfigs = new ObservableCollection<ConfigItemPresenter>();

            allConfigs.Add(new ConfigItemPresenter("OrderReportGreeting", "שורת ברכה בתחתית הסיכום"));
            allConfigs.Add(new ConfigItemPresenter("MegaUser", "שם משתמש מגה"));
            allConfigs.Add(new ConfigItemPresenter("MegaPassword", "סיסמא מגה"));
            allConfigs.Add(new ConfigItemPresenter("TemplateBasePath", "תיקית תבניות לדוחות"));
            allConfigs.Add(new ConfigItemPresenter("PackingPlanReportTemplatePath", "תבנית תוכנית אריזה"));
            allConfigs.Add(new ConfigItemPresenter("StickersTemplatePath", "תבנית מדבקות"));
            allConfigs.Add(new ConfigItemPresenter("BakerReportTemplatePath", "תבנית דוח לטבח"));
            allConfigs.Add(new ConfigItemPresenter("AllOrdersTemplatePath", "תבנית לדוח הזמנות -סיכום"));
            allConfigs.Add(new ConfigItemPresenter("OrderReportTemplatePath", "תבנית לדוח הזמנות -מפורט"));


        }
    }
}
