﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using MahApps.Metro.Controls;

namespace HolidayOrders.UI
{
    public class FocusOnFirstLoadedBehavior : Behavior<DataGrid>
    {
        private bool firstLoaded;
        protected override void OnAttached()
        {
            AssociatedObject.Loaded += AssociatedObject_Loaded;

        }

        private void AssociatedObject_Loaded(object sender, RoutedEventArgs e)
        {
            var dpd = DependencyPropertyDescriptor.FromProperty(ItemsControl.ItemsSourceProperty, typeof(DataGrid));
            if (dpd != null)
            {
                dpd.AddValueChanged(AssociatedObject, Handler);
            }
        }

        private void Handler(object sender, EventArgs eventArgs)
        {
            (AssociatedObject.ItemsSource as ObservableCollection<ItemWrapper>).CollectionChanged += FocusOnFirstLoadedBehavior_CollectionChanged;
        }

        private void FocusOnFirstLoadedBehavior_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            try
            {
                if (e.Action == NotifyCollectionChangedAction.Add)
                {
                    if (AssociatedObject.Items.Count > 1)
                    {
                        ComboBox comboBox;
                        var cell = AssociatedObject.GetCellByIndices(AssociatedObject.Items.Count - 1, -1);

                        if (cell != null)
                        {
                            comboBox = cell.FindVisualChild<ComboBox>();

                            comboBox.Focus();

                        }
                    }
                    else
                    {
                        //TODO:focus on first one 
                    }
                  
                }
            }
            catch (Exception ex)
            {
                
            }
          
        }
    }
}