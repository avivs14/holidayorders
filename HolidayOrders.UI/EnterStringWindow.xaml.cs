﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;

namespace HolidayOrders.UI
{
    /// <summary>
    /// Interaction logic for EnterStringWindow.xaml
    /// </summary>
    public partial class EnterStringWindow : MetroWindow
    {
        public EnterStringWindow(string title, string explainLine,string defualtValue)
        {
            Tag = defualtValue;
            InitializeComponent();
            Title = title;
            TextBlock.Text = explainLine;
            TextBox.Text = defualtValue;
            TextBox.Focus();

        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            this.Tag = TextBox.Text;
            this.Close();
        }
    }
}
