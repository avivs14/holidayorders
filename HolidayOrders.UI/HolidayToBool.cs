﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HolidayOrders.UI
{
    public class HolidayToBool:INotifyPropertyChanged
    {
        private Holiday _holiday;
        private bool _isSelected;

        public HolidayToBool(Holiday holiday, bool isSelected)
        {
            Holiday = holiday;
            IsSelected = isSelected;
        }

        public Holiday Holiday  
        {
            get { return _holiday; }
            set
            {
                _holiday = value;
                NotifyPropertyChanged("Holiday");
            }
        }

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                NotifyPropertyChanged("IsSelected");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string info)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(info));
        }

    }
}
