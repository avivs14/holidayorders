﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace HolidayOrders.UI
{
    public class IsUnitToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
            {
                bool isUnit = (bool)value;
                if (isUnit)
                {
                    return "יחידות";
                }
                return "משקל";
            }
            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var s = value as string;
            if (s != null)
            {
                string isUnitString = s;
                if (isUnitString == "יחידות")
                {
                    return true;
                }
                if (isUnitString == "משקל")
                {
                    return false;
                }
            }
            return false;
        }
    }
}