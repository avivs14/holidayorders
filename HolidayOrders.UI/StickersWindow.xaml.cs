﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.IO.Packaging;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Documents.Serialization;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Xps;
using System.Windows.Xps.Packaging;
using System.Threading.Tasks;
using CodeReason.Reports;
using MahApps.Metro.Controls;

namespace HolidayOrders.UI
{
    /// <summary>
    /// Application's main form
    /// </summary>
    public partial class StickersWindow : MetroWindow
    {
        private bool _firstActivated = true;
        private List<StickersPage> _stickersPages;
        internal static Settings Config = Settings.Default;



        public StickersWindow(List<StickersPage> stickersPages)
        {
            InitializeComponent();
            _stickersPages = stickersPages;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public StickersWindow()
        {
            InitializeComponent();
        }
        private void RemoveOldFiles()
        {
            var v = Directory.GetFiles("".AddCurrentDirectory());
            v.Where(x => x.Contains("tmp") || x.Contains("סיכום")).ToList().ForEach(x =>
            {
                try
                {
                    File.Delete(x);
                }
                catch (Exception)
                {
                }
            });
        }
        /// <summary>
        /// Window has been activated
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event details</param>
        public async Task ActivateWindow()
        {
            if (!_firstActivated) return;

            _firstActivated = false;
            await Task.Run(async () =>
            {
                try
                {
                    RemoveOldFiles();


                    ReportDocument reportDocument = new ReportDocument();

                    using (FileStream fs = new FileStream(Config.StickersTemplatePath, FileMode.Open, FileAccess.Read))
                    using (StreamReader reader = new StreamReader(fs))
                    {
                        reportDocument.XamlData = reader.ReadToEnd();
                        reportDocument.XamlImagePath = Path.Combine(Environment.CurrentDirectory, Config.TemplateBasePath);
                    }


                    var allXps = new List<XpsDocument>();
                    foreach (var stickerPage in _stickersPages)
                    {
                        if (stickerPage.AllLines.Any())
                        {
                            XpsDocument xps = CreateXpsFromStickerPage(stickerPage, reportDocument);
                            allXps.Add(xps);
                        }


                    }
                    var fileName = Path.ChangeExtension("סיכום" + Path.GetFileName(Path.GetTempFileName()), ".xps").AddCurrentDirectory();

                    fileName = Path.ChangeExtension(fileName, "xps");
                    await Application.Current.Dispatcher.InvokeAsync(() =>
                    {
                        allXps.MergeXpsDocumentAsync(fileName);
                        var mergedXpsDocument = new XpsDocument(fileName, FileAccess.ReadWrite);
                        documentViewer.Document = mergedXpsDocument.GetFixedDocumentSequence();
                        mergedXpsDocument.CleanPackageFromMemory();

                    });



                }
                catch (Exception ex)
                {
                    if (ex.Message == "FixedDocumentSequence must contain at least one FixedDocument.")
                    {
                        MessageBox.Show("לא נמצאו נתונים ");
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("ארעה תקלה בהכנת הדוח ");

                    }
                }
            });
        }

        private XpsDocument CreateXpsFromStickerPage(StickersPage stickersPage, ReportDocument reportDocument)
        {
            ReportData data = new ReportData();


            // sample table "Ean"
            DataTable table = new DataTable("Ean");
            table.Columns.Add("Position", typeof(string));
            table.Columns.Add("Item", typeof(string));
            table.Columns.Add("EAN", typeof(string));
            Random rnd = new Random(1234);
            foreach (var line in stickersPage.AllLines)
            {
                var thisLine = new List<string>();
                {
                    foreach (var cell in line.Line)
                    {
                        string formatted = String.Empty;
                        if (cell.Line1 != null)
                        {
                            formatted += cell.Line1;
                        }
                        formatted += "\n";

                        if (cell.Line2 != null)
                        {
                            formatted += cell.Line2;
                        }
                        formatted += "\n";

                        thisLine.Add(formatted);
                    }

                }
                table.Rows.Add(thisLine.ToArray());



                // randomly create some articles
            }
            data.DataTables.Add(table);

            DateTime dateTimeStart = DateTime.Now; // start time measure here

            var fileName =
                         Path.ChangeExtension(Path.Combine(Path.GetFileName(Path.GetTempFileName()).AddCurrentDirectory()),
                             ".xps");

            return reportDocument.CreateXpsDocument(data, fileName);
        }


        public void CreateXPSStreamPages(string targetDocument, List<string> list)
        {
            Package container = Package.Open(targetDocument, FileMode.Create);
            XpsDocument xpsDoc = new XpsDocument(container);
            XpsDocumentWriter writer = XpsDocument.CreateXpsDocumentWriter(xpsDoc);

            SerializerWriterCollator vxpsd = writer.CreateVisualsCollator();
            vxpsd.BeginBatchWrite();
            foreach (string sourceDocument in list)
            {
                AddXPSDocument(sourceDocument, vxpsd);
            }
            vxpsd.EndBatchWrite();
            container.Close();
        }

        public void AddXPSDocument(string sourceDocument, SerializerWriterCollator vxpsd)
        {
            using (XpsDocument xpsOld = new XpsDocument(sourceDocument, FileAccess.Read))
            {
                FixedDocumentSequence seqOld = xpsOld.GetFixedDocumentSequence();
                foreach (DocumentReference r in seqOld.References)
                {
                    FixedDocument d = r.GetDocument(false);
                    foreach (PageContent pc in d.Pages)
                    {
                        FixedPage fixedPage = pc.GetPageRoot(false);
                        double width = fixedPage.Width;
                        double height = fixedPage.Height;
                        Size sz = new Size(width, height);
                        fixedPage.Width = width;
                        fixedPage.Height = height;
                        fixedPage.Measure(sz);
                        fixedPage.Arrange(new Rect(new Point(), sz));
                        //fixedPage.UpdateLayout();

                        ContainerVisual newPage = new ContainerVisual();
                        newPage.Children.Add(fixedPage);
                        //test: add Watermark from Feng Yuan sample
                        //newPage.Children.Add(CreateWatermark(width, height, "Watermark"));

                        vxpsd.Write(newPage);
                    }
                }
            }

        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // Process.GetCurrentProcess().Kill();
        }
    }
}
