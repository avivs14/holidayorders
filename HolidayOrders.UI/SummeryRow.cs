﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HolidayOrders.UI
{
    public class SummeryRow
    {
        public string FoodType { get; set; }
        public double? TotalWeight { get; set; }
        public int? TotalUnits { get; set; }

        public int Box250 { get; set; }
        public int Box500 { get; set; }
        public int Box750 { get; set; }
        public int Box1000 { get; set; }
        public int Box1500 { get; set; }
        public int Box2000 { get; set; }
        public int Box3000 { get; set; }
        public int Box4000 { get; set; }

    }
}
    