﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CG.Web.MegaApiClient;
using HolidayOrders.UI;

namespace MonthSummery.UI
{
    public static class BackupManager
    {
        public static void BackupLocal()
        {

            try
            {

                var destPath = Path.ChangeExtension((@"Backups\DbBackup" + DateTime.Now.ToString("yyyy-mmmm-dd hh-MM")).AddCurrentDirectory(), "sdf");
                if (!Directory.Exists(Path.GetDirectoryName(destPath)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(destPath));
                }
                if (DB.Instance.Database.Connection.DataSource != null)
                {
                    File.Copy(DB.Instance.Database.Connection.DataSource, destPath, true);
                }
                var allFiles = new DirectoryInfo(Path.GetDirectoryName(destPath)).GetFiles();
                var orderedByDateFiles = allFiles.OrderBy(x => x.CreationTime);
                if (orderedByDateFiles.Count() > Settings.Default.BackupsToKeep)
                {
                    orderedByDateFiles.First().Delete();
                }

            }
            catch (Exception ex)
            {
            }

        }

        public static void BackupOnline()
        {
            Task.Run(() =>
            {
                try
                {
                    var config = Settings.Default;
                    var client = new MegaApiClient();

                    client.Login(config.MegaUser, config.MegaPassword);
                    var nodes = client.GetNodes();
                    var root = nodes.Single(n => n.Type == NodeType.Root);
                    var thisMonthFolder =
                        nodes.SingleOrDefault(n => n.Type == NodeType.Directory && n.Name == MonthFoler());
                    if (thisMonthFolder == null)
                    {
                        client.CreateFolder(MonthFoler(), root);
                        nodes = client.GetNodes();
                        thisMonthFolder =
                            nodes.SingleOrDefault(n => n.Type == NodeType.Directory && n.Name == MonthFoler());
                    }
                    if (thisMonthFolder != null)
                    {
                        var thisDayFolder =
                            nodes.SingleOrDefault(n => n.Name == Day() && n.ParentId == thisMonthFolder.Id);
                        if (thisDayFolder == null)
                        {
                            client.CreateFolder(Day(), thisMonthFolder);
                            nodes = client.GetNodes();
                            thisDayFolder =
                                nodes.SingleOrDefault(
                                    n =>
                                        n.Type == NodeType.Directory && n.Name == Day() &&
                                        n.ParentId == thisMonthFolder.Id);
                            if (thisDayFolder != null)
                            {
                                var seccess = false;
                                var destPath = Path.ChangeExtension("DbForUpload".AddCurrentDirectory(), "sdf");
                                if (DB.Instance.Database.Connection.DataSource != null)
                                {
                                    File.Copy(DB.Instance.Database.Connection.DataSource, destPath, true);
                                }
                                client.Upload(destPath, thisDayFolder);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                }
            });
        }

        private static string MonthFoler()
        {
            return DateTime.Now.ToString("MM-yyyy");
        }

        private static string Day()
        {
            return DateTime.Now.ToString("dd");
        }
    }
}