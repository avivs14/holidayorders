﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HolidayOrders.UI
{
    public class ItemWrapper:INotifyPropertyChanged
    {
        public Item Item
        {
            get { return _item; }
            set
            {
                _item = value;
                NotifyPropertyChanged("Item");
            }
        }

        private int _itemNumber;
        private Item _item;

        public ItemWrapper(Item item)
        {
            this.Item = item;
        }

        public int ItemNumber
        {
            get { return _itemNumber; }
            set
            {
                _itemNumber = value;
                NotifyPropertyChanged("ItemNumber");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string info)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(info));
        }
    }
}
