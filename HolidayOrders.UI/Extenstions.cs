﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Packaging;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Xps;
using System.Windows.Xps.Packaging;
using System.Xml.Schema;

namespace HolidayOrders.UI
{
    public static class Extenstions
    {
        public static void CleanPackageFromMemory(this XpsDocument xpsDocument)
        {
            //Get the Uri from which the system opened the XpsPackage and so your XpsDocument
            var myXpsUri = xpsDocument.Uri; //should point to the same file as tempXpsFile

            //Get the XpsPackage itself
            var theXpsPackage = PackageStore.GetPackage(myXpsUri);

            //THIS IS THE KEY!!!! close it and make it let go of it's file locks
            theXpsPackage.Close();

            //if you don't remove the package from the PackageStore, you won't be able to
            //re-open the same file again later (due to System.IO.Packaging's Package store/caching
            //rather than because of any file locks)
            PackageStore.RemovePackage(myXpsUri);
        }


        public static void MergeXpsDocumentAsync(this List<XpsDocument> sourceDocuments, string newFile)
        {

            if (File.Exists(newFile))
            {
                File.Delete(newFile);
            }

            using (XpsDocument xpsDocument = new XpsDocument(newFile, System.IO.FileAccess.ReadWrite))
            {
                XpsDocumentWriter xpsDocumentWriter = XpsDocument.CreateXpsDocumentWriter(xpsDocument);
                FixedDocumentSequence fixedDocumentSequence = new FixedDocumentSequence();

                foreach (XpsDocument doc in sourceDocuments)
                {
                    FixedDocumentSequence sourceSequence = doc.GetFixedDocumentSequence();
                    foreach (DocumentReference dr in sourceSequence.References)
                    {
                        DocumentReference newDocumentReference = new DocumentReference();
                        newDocumentReference.Source = dr.Source;
                        (newDocumentReference as IUriContext).BaseUri = (dr as IUriContext).BaseUri;
                        FixedDocument fd = newDocumentReference.GetDocument(true);
                        newDocumentReference.SetDocument(fd);
                        fixedDocumentSequence.References.Add(newDocumentReference);
                    }
                }
                xpsDocumentWriter.Write(fixedDocumentSequence);
            }

        }


        public static string AddCurrentDirectory(this string originalString)
        {
            return Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
                originalString);
        }

        public static IList<PackingPage> AsPackingPages(this IList<Food> foods, IList<Holiday> holidaysToSum)
        {
            var holidayIds = holidaysToSum.Select(x => x.Id).ToList();
            var holidays = DB.Instance.HolidaySet.Where(x => holidayIds.Contains(x.Id));
            if (holidays.Any())
            {
                var queryResult = holidays.SelectMany(x => x.Orders).SelectMany(x => x.Items)
                    .GroupBy(x => x.Food)
                    .OrderBy(x => x.Key.Name)
                    .ToList()
                    .Select(
                        x =>
                            new PackingPage(string.Join(",", holidaysToSum.Select(y => y.Name)), x.Key.Name, x.ToPackingRows().OrderByDescending(y => y.HowManyBoxes).ToList()));
                return queryResult.ToList();
            }
            return Enumerable.Empty<PackingPage>().ToList();
        }


        public static List<StickersLine> ToStickerLines(this IEnumerable<IGrouping<Food, Item>> FoodToItems, StickersStackingStrategy strategy)
        {
            var linesToReturn = new List<StickerCell>();

            if (strategy == StickersStackingStrategy.NoSpace)
            {
                var items = FoodToItems.SelectMany(x => x);
                foreach (var item in items)
                {
                    var cell = item.ToStickerCell(item.Food.Name);
                    linesToReturn.Add(cell);
                }
            }
            else
            {
                foreach (var items in FoodToItems)
                {
                    foreach (var item in items)
                    {
                        var cell = item.ToStickerCell(items.Key.Name);
                        linesToReturn.Add(cell);
                    }
                }
            }

            return linesToReturn.ToLines(3, strategy);
        }

        public static IList<StickersPage> AsStickersPages(this IList<Food> foods, IList<Holiday> holiday, StickersStackingStrategy strategy, bool isPrintAllFoods, Food foodToPrint)
        {
            var holidayIds = holiday.Select(x => x.Id).ToList();

            var holidays = DB.Instance.HolidaySet.Where(x => holidayIds.Contains(x.Id));

            if (holidays.Any())
            {
                IEnumerable<IGrouping<Food, Item>> queryResult;
                if (isPrintAllFoods)
                {
                    queryResult = holidays.SelectMany(x => x.Orders).SelectMany(x => x.Items)
                                        .GroupBy(x => x.Food).OrderBy(y => y.Key.Name);
                }
                else
                {
                    queryResult = holidays.SelectMany(x => x.Orders).SelectMany(x => x.Items.Where(y => y.FoodId == foodToPrint.Id))
                                         .GroupBy(x => x.Food);

                }

                if (strategy == StickersStackingStrategy.PageForEech)
                {
                    return queryResult.Select(
                        x =>
                            new StickersPage() { AllLines = x.ToStickerLines(x.Key.Name, strategy) }).ToList();
                }
                if (strategy == StickersStackingStrategy.NoSpace)
                {
                    return new List<StickersPage>()
                    {
                        new StickersPage()
                        {
                            AllLines = queryResult.ToStickerLines(strategy)
                        }
                    };
                }
                if (strategy == StickersStackingStrategy.OneRowSpace)
                {
                    return new List<StickersPage>()
                    {
                        new StickersPage()
                        {
                            AllLines = queryResult.ToStickerLines(strategy)
                        }
                    };
                }
            }
            return Enumerable.Empty<StickersPage>().ToList();
        }



        public static List<StickersLine> ToStickerLines(this IEnumerable<Item> items, string foodName, StickersStackingStrategy strategy)
        {
            var linesToReturn = new List<StickerCell>();
            foreach (var item in items)
            {
                var cell = item.ToStickerCell(foodName);
                linesToReturn.Add(cell);
            }
            return linesToReturn.ToLines(3, strategy);
        }

        public static List<StickersLine> ToLines(this IEnumerable<StickerCell> items, int cellsInLine, StickersStackingStrategy strategy)
        {
            var toReturn = new List<StickersLine>();
            var innerItems = items.ToList();
            if (strategy == StickersStackingStrategy.OneRowSpace)
            {
                var groupedInnerItems = innerItems.GroupBy(x => x.Line1);
                foreach (var groupedInnerItem in groupedInnerItems)
                {
                    var itemsFromThisGroup = groupedInnerItem.ToList();
                    while (itemsFromThisGroup.Count() >= cellsInLine)
                    {
                        toReturn.Add(new StickersLine() { Line = itemsFromThisGroup.Take(cellsInLine).ToList() });
                        itemsFromThisGroup = itemsFromThisGroup.Skip(cellsInLine).ToList();
                    }
                    toReturn.Add(new StickersLine() { Line = itemsFromThisGroup.ToList() });

                    toReturn.Add(new StickersLine()
                    {
                        Line = new List<StickerCell>()
                        {
                            new StickerCell()
                            {
                                Line1 = "-----------------------------",
                                Line2 = "-----------------------------"
                            }
                            ,   new StickerCell()
                            {
                                Line1 = "-----------------------------",
                                Line2 = "-----------------------------"
                            },   new StickerCell()
                            {
                                Line1 = "-----------------------------",
                                Line2 = "-----------------------------"
                            }
                        }
                    });

                }
            }
            else
            {
                while (innerItems.Count() >= cellsInLine)
                {
                    toReturn.Add(new StickersLine() { Line = innerItems.Take(cellsInLine).ToList() });
                    innerItems = innerItems.Skip(cellsInLine).ToList();
                }
                toReturn.Add(new StickersLine() { Line = innerItems.ToList() });
            }


            return toReturn;
        }

        public static StickerCell ToStickerCell(this Item item, string foodName)
        {
            var toReturn = new StickerCell();

            toReturn.Line1 = foodName;

            if (!item.IsUnits)
            {
                var isBoxOf = item.IsBoxOf ? "קופסא של " : string.Empty;
                toReturn.Line2 = isBoxOf + item.Box.DisplayWeight + " " + item.Box.DisplayUnits;
            }
            else
            {
                toReturn.Line2 = item.Units.ToDisplayUnits() + " " + "יחידות";
            }
            toReturn.Line2 += string.Format(" להזמנה {1}{0} ", item.Order.OrderNumber, item.Order.Holiday.Symbol);
            return toReturn;
        }

        public static List<PackingRow> ToPackingRows(this IEnumerable<Item> items)
        {

            var numberAndUnit = new List<Tuple<string, string>>();

            items.ToList().ForEach(x =>
            {
                if (x.IsUnits)
                {
                    numberAndUnit.Add(new Tuple<string, string>(x.Units.ToString(), "יחידות"));
                }
                else
                {
                    var isBoxOf = x.IsBoxOf ? "קופסא של " : string.Empty;
                    numberAndUnit.Add(new Tuple<string, string>(isBoxOf + x.Box.DisplayWeight, x.Box.DisplayUnits));
                }
            });

            return
                numberAndUnit.GroupBy(x => new { x.Item1, x.Item2 })
                    .Select(x => new PackingRow(x.Count(), x.Key.Item1, x.Key.Item2)).ToList();
        }



        public static IList<SummeryRow> ToSummeryRow(this ICollection<Holiday> holidaysToSum)
        {
            var idsList = holidaysToSum.Select(x => x.Id);
            var holidays = DB.Instance.HolidaySet.Where(x => idsList.Contains(x.Id));
            if (holidays.Any())
            {
                var queryResult = holidays.SelectMany(x => x.Orders).SelectMany(x => x.Items)
                    .GroupBy(x => x.Food)
                    .OrderBy(x => x.Key.Name)
                    .Select(
                        x =>
                            new SummeryRow()
                            {
                                FoodType = x.Key.Name,
                                TotalUnits = x.Where(z => z.IsUnits).Sum(y => (int?)y.Units),
                                TotalWeight = x.Where(z => !z.IsUnits).Sum(y => (double?)y.Box.Weight),
                                Box250 = x.Count(z => !z.IsUnits && z.Box.Weight == 250),
                                Box500 = x.Count(z => !z.IsUnits && z.Box.Weight == 500),
                                Box750 = x.Count(z => !z.IsUnits && z.Box.Weight == 750),
                                Box1000 = x.Count(z => !z.IsUnits && z.Box.Weight == 1000),
                                Box1500 = x.Count(z => !z.IsUnits && z.Box.Weight == 1500),
                                Box2000 = x.Count(z => !z.IsUnits && z.Box.Weight == 2000),
                                Box3000 = x.Count(z => !z.IsUnits && z.Box.Weight == 3000),
                                Box4000 = x.Count(z => !z.IsUnits && z.Box.Weight == 4000),


                            });
                return queryResult.ToList();
            }
            return Enumerable.Empty<SummeryRow>().ToList();
        }

        public static string ToDisplayWeight(this double originalDouble)
        {
            var asKG = originalDouble / 1000;
            return asKG.ToString("N2");
        }
        public static string ToDisplayUnits(this int originalInt)
        {
            return originalInt.ToString("N0");
        }

        public static string ToDisplayWeightNulleble(this double? originalDouble)
        {
            if (originalDouble.HasValue)
            {
                var asKG = originalDouble / 1000;
                return asKG.Value.ToString("N2");
            }
            return "0";
        }
        public static string ToDisplayUnitsNulleble(this int? originalInt)
        {
            if (originalInt.HasValue)
            {
                return originalInt.Value.ToString("N0");
            }
            return "0";
        }

    }
}
