﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO.Packaging;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Prism.Commands;

namespace HolidayOrders.UI
{
    public class MainPresenter : INotifyPropertyChanged
    {
        private SummeryStrategy _summeryStrategy;
        public SummeryStrategy SummeryStrategy
        {
            get { return _summeryStrategy; }
            set
            {
                _summeryStrategy = value;
                NotifyPropertyChanged("SummeryStrategy");
            }
        }

        public StickersStackingStrategy StickerslLineStackingStrategy
        {
            get { return _stickerslLineStackingStrategy; }
            set
            {
                _stickerslLineStackingStrategy = value;
                NotifyPropertyChanged("StickerslLineStackingStrategy");
            }
        }

        public Holiday TargetHoliday
        {
            get { return _targetHoliday; }
            set
            {
                _targetHoliday = value;
                if (TargetHoliday != SelectedHoliday)
                {
                    MoveSelectedOrderToAnotherHoliday();
                }
                _targetHoliday = SelectedHoliday;
                NotifyPropertyChanged("TargetHoliday");
            }
        }

        public DelegateCommand MoveSelectedOrderToAnotherHolidayCommand { get; set; }
        public ObservableCollection<Holiday> AllHolidays
        {
            get { return _allHolidays; }
            set
            {
                _allHolidays = value;
                NotifyPropertyChanged("AllHolidays");
            }
        }

        public ObservableCollection<HolidayToBool> AllHolidaysToBools
        {
            get { return _allHolidaysToBools; }
            set
            {
                _allHolidaysToBools = value;
                NotifyPropertyChanged("AllHolidaysToBools");
            }
        }

        public string SearchString
        {
            get { return _searchString; }
            set
            {
                _searchString = value;
                NotifyPropertyChanged("SearchString");
                FilterOrders();

            }
        }

        public string Title
        {
            get
            {
                return SelectedHoliday != null ? string.Format("עריכת חג - {0} ", SelectedHoliday.Name) : "";
            }
        }
        private void FilterOrders()
        {
            if (AllOrders != null)
            {
                foreach (var orderWrapper in AllOrders)
                {
                    if (!string.IsNullOrWhiteSpace(SearchString))
                    {
                        if (orderWrapper.CustomerName.Contains(SearchString))
                        {
                            orderWrapper.IsVisible = true;
                        }
                        else
                        {
                            orderWrapper.IsVisible = false;
                        }
                    }
                    else
                    {
                        orderWrapper.IsVisible = true;
                    }
                }
            }

        }

        public bool IsLoadingItems
        {
            get { return _isLoadingItems; }
            set
            {
                _isLoadingItems = value;
                NotifyPropertyChanged("IsLoadingItems");
            }
        }

        private ObservableCollection<OrderWrapper> _allOrders;
        private ObservableCollection<ItemWrapper> _presentedItems;
        private OrderWrapper _selectedOrder;
        private Holiday _selectedHoliday;
        private ItemWrapper _selectedItem;
        private ObservableCollection<Food> _possibleFoods;
        private ObservableCollection<Box> _possibleBoxes;
        private bool _isLoadingItems;
        private string _searchString;
        private ObservableCollection<Holiday> _allHolidays;
        private Holiday _targetHoliday;
        private StickersStackingStrategy _stickerslLineStackingStrategy;
        private bool _isPrintAllFoods;
        private Food _foodToStickerPrint;
        private ObservableCollection<HolidayToBool> _allHolidaysToBools;
        private NetworkConnectionNotificationManager _networkConnectionNotificationManager;
        public bool IsEditItemsEnabled
        {
            get
            {
                if (AllOrders != null && PossibleFoods != null)
                {
                    return AllOrders.Any() && PossibleFoods.Any() && SelectedOrder != null;
                }
                return false;
            }
        }

        private void RaiseEditItemsEnabledPropertyChanged()
        {
            NotifyPropertyChanged("IsEditItemsEnabled");
        }

        public DelegateCommand PrintPackingPlanReportCommand { get; set; }
        public DelegateCommand PrintBakerReportCommand { get; set; }
        public DelegateCommand PrintOrderCommand { get; set; }
        public DelegateCommand ItemMightChangedCommand { get; set; }
        public DelegateCommand PrintAllOrdersListCommand { get; set; }
        public DelegateCommand OrderCommentsChangedCommand { get; set; }
        public DelegateCommand OrderPhoneChangedCommand { get; set; }
        public DelegateCommand PrintStickersCommand { get; set; }

        public ObservableCollection<Box> PossibleBoxes
        {
            get { return _possibleBoxes; }
            set
            {
                _possibleBoxes = value;
                NotifyPropertyChanged("PossibleBoxes");
            }
        }

        public ObservableCollection<Food> PossibleFoods
        {
            get { return _possibleFoods; }
            set
            {
                _possibleFoods = value;
                NotifyPropertyChanged("PossibleFoods");
                RaiseEditItemsEnabledPropertyChanged();
                _possibleFoods.CollectionChanged += PossibleFoods_CollectionChanged;
            }
        }
        private void HandleItemMightChange()
        {
            if (SelectedItem != null)
            {
                var db = DB.Instance;
                var itemOrNull = db.Items.SingleOrDefault(x => x.Id == SelectedItem.Item.Id);
                if (itemOrNull != null)
                {
                    if (SelectedItem.Item.Food == null)
                    {
                        itemOrNull.FoodId = PossibleFoods.First().Id;
                        MessageBox.Show("שים לב,פריט לא קיים");
                    }

                    else if (itemOrNull.FoodId != SelectedItem.Item.Food.Id)
                    {
                        itemOrNull.FoodId = SelectedItem.Item.Food.Id;
                    }

                    if (itemOrNull.BoxId != SelectedItem.Item.Box.Id)
                    {
                        itemOrNull.BoxId = SelectedItem.Item.Box.Id;
                    }

                    if (itemOrNull.Units != SelectedItem.Item.Units)
                    {
                        itemOrNull.Units = SelectedItem.Item.Units;
                    }

                    if (itemOrNull.IsUnits != SelectedItem.Item.IsUnits)
                    {
                        itemOrNull.IsUnits = SelectedItem.Item.IsUnits;
                    }

                    if (itemOrNull.IsBoxOf != SelectedItem.Item.IsBoxOf)
                    {
                        itemOrNull.IsBoxOf = SelectedItem.Item.IsBoxOf;
                    }
                }
                DB.Save(db);
            }
        }

        public DelegateCommand OpenEditFoodWindowCommand { get; set; }
        public DelegateCommand PrintAllOrdersCommand { get; set; }
        public DelegateCommand AddOrderCommand { get; set; }
        public DelegateCommand AddItemCommand { get; set; }
        public DelegateCommand DeleteOrderCommand { get; set; }
        public DelegateCommand DeleteItemCommand { get; set; }
        public DelegateCommand RenameOrderCommand { get; set; }
        public Holiday SelectedHoliday
        {
            get { return _selectedHoliday; }
            set
            {
                _selectedHoliday = value;
                var firstOrDefault = DB.Instance.HolidaySet.FirstOrDefault(x => x.Id == SelectedHoliday.Id);
                if (firstOrDefault != null)
                {
                    AllOrders = new ObservableCollection<OrderWrapper>(firstOrDefault.Orders.OrderBy(x => x.OrderNumber).ToList().Select(x => new OrderWrapper(x) { IsVisible = true }));
                }
                SelectedOrder = AllOrders.FirstOrDefault();

                var selected = AllHolidaysToBools.FirstOrDefault(x => x.Holiday.Id == _selectedHoliday.Id);
                if (selected != null)
                {
                    selected.IsSelected = true;
                }

                NotifyPropertyChanged("SelectedHoliday");
                NotifyPropertyChanged("Title");

            }
        }

        public ObservableCollection<OrderWrapper> AllOrders
        {
            get { return _allOrders; }
            set
            {
                _allOrders = value;
                NotifyPropertyChanged("AllOrders");
                RaiseEditItemsEnabledPropertyChanged();
                _allOrders.CollectionChanged += PossibleFoods_CollectionChanged;
                _allOrders.CollectionChanged += SortNumbersWhenCollectionChanged;
            }
        }

        private void SortNumbersWhenCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            sortNumbers(sender as ObservableCollection<OrderWrapper>);
        }

        private static void sortNumbers(ObservableCollection<OrderWrapper> allOrders)
        {
            if (allOrders != null)
            {
                var db = DB.Instance;
                for (int i = 0; i < allOrders.Count; i++)
                {
                    var order = allOrders[i];
                    if (order.OrderNumber != i + 1)
                    {
                        order.OrderNumber = i + 1;

                        var DBorder = db.Orders.SingleOrDefault(x => x.Id == order.Id);
                        if (DBorder != null)
                        {
                            DBorder.OrderNumber = order.OrderNumber;
                        }
                    }
                }

                DB.Save(db);
            }
        }

        public OrderWrapper SelectedOrder
        {
            get { return _selectedOrder; }
            set
            {
                _selectedOrder = value;
                UpdatePresentedItems();
                NotifyPropertyChanged("SelectedOrder");
                RaiseEditItemsEnabledPropertyChanged();

            }
        }


        public ItemWrapper SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                NotifyPropertyChanged("SelectedItem");
            }
        }

        public ObservableCollection<ItemWrapper> PresentedItems
        {
            get { return _presentedItems; }
            set
            {
                _presentedItems = value;
                NotifyPropertyChanged("PresentedItems");

            }
        }
        public async void OnInternetConnectionIsActiveChanged(bool isActive)
        {

            if (isActive)
            {
                await WindowHelper.HideIsInternetAvaliable();
            }
            else
            {
                await WindowHelper.ShowIsInternetAvaliable();
            }
        }

        public MainPresenter()
        {
            _networkConnectionNotificationManager = new NetworkConnectionNotificationManager();
            _networkConnectionNotificationManager.IsActive.Subscribe(OnInternetConnectionIsActiveChanged);
            InitializeCommands();
            InitializePossibleFoods();
            IsPrintAllFoods = true;
            IsLoadingItems = false;
            SearchString = string.Empty;
            PossibleBoxes = new ObservableCollection<Box>(DB.Instance.BoxSet.ToList());
            AllHolidays = new ObservableCollection<Holiday>(DB.Instance.HolidaySet);
            AllHolidaysToBools =
                new ObservableCollection<HolidayToBool>(DB.Instance.HolidaySet.ToList()
                    .Select(x => new HolidayToBool(x, false)));
            SummeryStrategy = SummeryStrategy.ThisHolidayOnly;

        }

        private void InitializePossibleFoods()
        {
            PossibleFoods = new ObservableCollection<Food>(DB.Instance.Foods.ToList().OrderBy(x => x.Name));

            var pleaseChooseItem = PossibleFoods.FirstOrDefault(x => x.Name.Contains("בחר פריט"));

            if (pleaseChooseItem != null)
            {
                var pleaseChooseItemIndex = PossibleFoods.IndexOf(pleaseChooseItem);
                PossibleFoods.Move(pleaseChooseItemIndex, 0);
            }
            FoodToStickerPrint = PossibleFoods.First();
            if (SelectedOrder != null)
            {
                SelectedOrder = _selectedOrder;
            }

        }

        private void PossibleFoods_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            RaiseEditItemsEnabledPropertyChanged();
        }

        private void RenameOrder()
        {
            var db = DB.Instance;
            var order = db.Orders.SingleOrDefault(x => x.Id == SelectedOrder.Id);
            if (order != null)
            {
                order.CustomerName = SelectedOrder.CustomerName;
            }

            DB.Save(db);
            //SelectedHoliday = _selectedHoliday;

        }

        private void InitializeCommands()
        {
            AddItemCommand = new DelegateCommand(AddItem);
            AddOrderCommand = new DelegateCommand(AddOrder);
            DeleteItemCommand = new DelegateCommand(DeleteItem);
            DeleteOrderCommand = new DelegateCommand(DeleteOrder);
            ItemMightChangedCommand = new DelegateCommand(HandleItemMightChange);
            RenameOrderCommand = new DelegateCommand(RenameOrder);
            PrintOrderCommand = new DelegateCommand(() => { PrintOrders(new List<OrderWrapper> { SelectedOrder }); });
            PrintAllOrdersCommand = new DelegateCommand(() => { PrintOrders(AllOrders); });
            PrintAllOrdersListCommand = new DelegateCommand(PrintAllOrdersList);
            OrderCommentsChangedCommand = new DelegateCommand(OrderCommentsChanged);
            OrderPhoneChangedCommand = new DelegateCommand(OrderPhoneChanged);
            PrintBakerReportCommand = new DelegateCommand(PrintBakerReport);
            OpenEditFoodWindowCommand = new DelegateCommand(OpenEditFoodWindow);
            PrintPackingPlanReportCommand = new DelegateCommand(PrintPackingPlanReport);
            PrintStickersCommand = new DelegateCommand(PrintStickers);
            MoveSelectedOrderToAnotherHolidayCommand = new DelegateCommand(MoveSelectedOrderToAnotherHoliday);
        }

        private void MoveSelectedOrderToAnotherHoliday()
        {
            var db = DB.Instance;
            var orderOrNull = db.Orders.FirstOrDefault(x => x.Id == SelectedOrder.Id);
            if (orderOrNull != null)
            {
                orderOrNull.HolidayId = TargetHoliday.Id;
            }
            var selectedIndex = AllOrders.IndexOf(SelectedOrder);
            AllOrders.Remove(SelectedOrder);
            DB.Save(db);
            SelectClosestItem(selectedIndex);
            sortNumbers(AllOrders);
        }

        public Food FoodToStickerPrint
        {
            get { return _foodToStickerPrint; }
            set
            {
                _foodToStickerPrint = value;
                NotifyPropertyChanged("FoodToStickerPrint");
            }
        }

        private async void PrintStickers()
        {
            IsLoadingItems = true;
            StickersWindow window = new StickersWindow(DB.Instance.Foods.ToList().AsStickersPages(GetHolidaysToPrint(), StickerslLineStackingStrategy, IsPrintAllFoods, FoodToStickerPrint).ToList());
            await window.ActivateWindow();
            window.Show();

            IsLoadingItems = false;
        }

        private List<Holiday> GetHolidaysToPrint()
        {
            var holidaysToPrint = new List<Holiday>();
            if (SummeryStrategy == SummeryStrategy.ThisHolidayOnly)
            {
                holidaysToPrint.Add(SelectedHoliday);
            }
            else if (SummeryStrategy == SummeryStrategy.SumOtherHolidays)
            {
                holidaysToPrint.AddRange(AllHolidaysToBools.Where(x => x.IsSelected).Select(x => x.Holiday));
            }
            return holidaysToPrint;
        }

        private async void PrintPackingPlanReport()
        {
            IsLoadingItems = true;

            PrintPackingPlanWindow window = new PrintPackingPlanWindow(DB.Instance.Foods.ToList().AsPackingPages(GetHolidaysToPrint()));
            await window.ActivateWindow();
            window.Show();

            IsLoadingItems = false;

        }

        private void OpenEditFoodWindow()
        {
            EditFoodWindow window = new EditFoodWindow();
            window.ShowDialog();
            InitializePossibleFoods();
        }

        private async void PrintBakerReport()
        {
            IsLoadingItems = true;

            string holidayNameOrNames = SelectedHoliday.Name;
            if (SummeryStrategy == UI.SummeryStrategy.SumOtherHolidays)
            {
                holidayNameOrNames = String.Join(",",
                    AllHolidaysToBools.Where(x => x.IsSelected).Select(x => x.Holiday.Name));
            }

            var bakerReportWindow = new PrintBakerReport(GetHolidaysToPrint().ToSummeryRow(), holidayNameOrNames);
            await bakerReportWindow.ActivateWindow();
            bakerReportWindow.Show();

            IsLoadingItems = false;
        }

        private void OrderPhoneChanged()
        {
            if (string.IsNullOrEmpty(SelectedOrder.Phone) == false)
            {
                var db = DB.Instance;
                var singleOrDefault = db.Orders.SingleOrDefault(x => x.Id == SelectedOrder.Id);
                if (singleOrDefault != null)
                {
                    singleOrDefault.Phone = SelectedOrder.Phone;
                    DB.Save(db);
                }
            }
        }

        private void OrderCommentsChanged()
        {
            if (string.IsNullOrEmpty(SelectedOrder.Comments) == false)
            {
                var db = DB.Instance;
                var singleOrDefault = db.Orders.SingleOrDefault(x => x.Id == SelectedOrder.Id);
                if (singleOrDefault != null)
                {
                    singleOrDefault.Comments = SelectedOrder.Comments;
                    DB.Save(db);
                }
            }
        }

        private async void PrintAllOrdersList()
        {

            IsLoadingItems = true;

            PrintAllOrdersListWindow window = new PrintAllOrdersListWindow(AllOrders.Select(x => x.Order).ToList());
            await window.ActivateWindow();
            window.Show();

            IsLoadingItems = false;


        }

        private async void PrintOrders(IList<OrderWrapper> orders)
        {

            IsLoadingItems = true;

            var printOrdersWindow = new PrintOrdersWindow(orders.Select(x => x.Order).ToList());
            await printOrdersWindow.ActivateWindow();
            printOrdersWindow.Show();

            IsLoadingItems = false;

        }

        private async void DeleteOrder()
        {
            var metroWindow = Application.Current.Windows.Cast<Window>().First(x => x is MainWindow) as MetroWindow;
            var settings = new MetroDialogSettings();
            settings.NegativeButtonText = "כן, אני בטוח";
            settings.AffirmativeButtonText = "ביטול";
            var result = await metroWindow.ShowMessageAsync("האם אתה בטוח?",
                "מחיקה של הזמנה תמחק את כל הפריטים שבתוכה , האם אתה בטוח שברצונך למחוק הזמנה זו ?",
                MessageDialogStyle.AffirmativeAndNegative, settings);

            if (result == MessageDialogResult.Negative)
            {
                int selectedIndex = AllOrders.IndexOf(SelectedOrder);
                var db = DB.Instance;
                var order = db.Orders.SingleOrDefault(x => x.Id == SelectedOrder.Id);
                db.Orders.Remove(order);
                AllOrders.Remove(SelectedOrder);

                SelectClosestItem(selectedIndex);
                DB.Save(db);

            }

        }

        private void SelectClosestItem(int selectedIndex)
        {
            if (AllOrders.Any())
            {
                if (selectedIndex > 0)
                {
                    selectedIndex--;
                }
                SelectedOrder = AllOrders[selectedIndex];
            }
        }

        private void DeleteItem()
        {
            var db = DB.Instance;
            var item = db.Items.SingleOrDefault(x => x.Id == SelectedItem.Item.Id);
            db.Items.Remove(item);

            PresentedItems.Remove(SelectedItem);
            DB.Save(db);

        }

        private async void AddOrder()
        {
            var metroWindow = Application.Current.Windows.Cast<Window>().First(x => x is MainWindow) as MetroWindow;

            var settings = new MetroDialogSettings();
            settings.AffirmativeButtonText = "אישור";
            settings.NegativeButtonText = "ביטול";
            settings.DefaultText = "בחר שם להזמנה";

            var result = await metroWindow.ShowInputAsync("הוספת הזמנה חדשה", "של מי ההזמנה שתרצה להוסיף?", settings);
            if (result != null)
            {
                var db = DB.Instance;
                var firstOrDefault = db.HolidaySet.FirstOrDefault(x => x.Id == SelectedHoliday.Id);
                if (firstOrDefault != null)
                {
                    var orders = firstOrDefault.Orders;
                    var orderNumber = 1;
                    if (orders.Any())
                    {
                        orderNumber = firstOrDefault.Orders.Max(x => x.OrderNumber) + 1;
                    }
                    var newOrder = new Order()
                    {
                        CustomerName = result,
                        HolidayId = SelectedHoliday.Id,
                        OrderNumber = orderNumber,
                        Comments = string.Empty,
                        Phone = string.Empty,
                    };
                    db.Orders.Add(newOrder);
                    DB.Save(db);

                    AllOrders.Add(new OrderWrapper(DB.Instance.Orders.FirstOrDefault(x => x.Id == newOrder.Id)));
                    (metroWindow as MainWindow).OrderAdded();
                    AddItem();
                    NotifyPropertyChanged("AllOrders");

                }
            }


        }

        public bool IsPrintAllFoods
        {
            get { return _isPrintAllFoods; }
            set
            {
                _isPrintAllFoods = value;
                NotifyPropertyChanged("IsPrintAllFoods");
            }
        }

        private void AddItem()
        {
            var db = DB.Instance;
            if (PossibleBoxes.Any() && PossibleFoods.Any())
            {
                var newItem = new Item()
                {
                    BoxId = PossibleBoxes.First().Id,
                    FoodId = PossibleFoods.First().Id,
                    OrderId = SelectedOrder.Id,
                    Units = 1,
                    IsUnits = false,
                    IsBoxOf = false,
                };

                db.Items.Add(newItem);
                DB.Save(db);

                PresentedItems.Add(new ItemWrapper(DB.Instance.Items.FirstOrDefault(x => x.Id == newItem.Id)));



            }


        }

        private async void UpdatePresentedItems()
        {
            IsLoadingItems = true;
            await Task.Run(() =>
            {
                if (SelectedOrder != null)
                {
                    var order = DB.Instance.Orders.FirstOrDefault(x => x.Id == SelectedOrder.Id);
                    if (order != null)
                    {
                        try
                        {
                            if (PresentedItems != null)
                            {
                                PresentedItems.CollectionChanged -= NotifyItemsChanged;
                            }

                        }
                        catch (Exception)
                        {
                        }
                        PresentedItems = new ObservableCollection<ItemWrapper>(order.Items.ToList().Select(x => new ItemWrapper(x)));
                        SortItemsNumbers();
                        PresentedItems.CollectionChanged += NotifyItemsChanged;
                    }
                }
            });
            IsLoadingItems = false;

        }

        private void NotifyItemsChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            SortItemsNumbers();
        }

        private void SortItemsNumbers()
        {
            for (int i = 0; i < PresentedItems.Count; i++)
            {
                var item = PresentedItems[i];
                if (item.ItemNumber != i + 1)
                {
                    item.ItemNumber = i + 1;
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string info)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(info));
        }
    }
}
