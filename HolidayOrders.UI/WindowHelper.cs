﻿using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;

namespace HolidayOrders.UI
{
    public static class WindowHelper
    {
        public static async Task HideIsInternetAvaliable()
        {
            RunOnUiThread(() =>
            {
                var window = GetRootMetroWindow();
                try
                {
                    window.HideMetroDialogAsync(NoInternetDialog);
                }
                catch (Exception ex) { };
            });

        }

        public static MetroWindow GetRootMetroWindow()
        {
            return Application.Current.Windows.OfType<MetroWindow>().First();
        }
        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }
        private static CustomDialog _noInternetDialog;
        static CustomDialog NoInternetDialog
        {
            get
            {
                if (_noInternetDialog != null)
                {
                    return _noInternetDialog;
                }
                _noInternetDialog = new CustomDialog() { Content = "לא ניתן להשתמש בתוכנה ללא חיבור לאינטרנט", Title = "החיבור לאינטרנט אבד" };
                return _noInternetDialog;
            }
        }
        public static MainPresenter GetMainPresenter()
        {
            var window = GetRootMetroWindow();
            return (MainPresenter)window.DataContext;

        }
        public static void RunOnUiThread(Action action)
        {
            System.Windows.Application.Current.Dispatcher.Invoke(action);
        }
        public static async Task ShowIsInternetAvaliable()
        {
            RunOnUiThread(() =>
            {
                var window = GetRootMetroWindow();
                //await ShowUserMessageAsync(title, message, buttonOkText, window);
                 window.ShowMetroDialogAsync(NoInternetDialog);
            });
            

        }



        public static async Task<string> OpenInputDialog(string message, string affirmativeText, string negativeText)
        {
            var window = GetRootMetroWindow();

            var result = await window.ShowInputAsync("לתשומת ליבך!", message, new MetroDialogSettings()
            {
                AffirmativeButtonText = affirmativeText,
                NegativeButtonText = negativeText
            });
            return result;
        }
    }
}
