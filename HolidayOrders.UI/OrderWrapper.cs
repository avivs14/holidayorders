﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HolidayOrders.UI
{
    public class OrderWrapper:INotifyPropertyChanged
    {
        private Order _order;
        private bool _isVisible;

        public Order Order
        {
            get { return _order; }
            set
            {
                _order = value;
                NotifyPropertyChanged("Order");
            }
        }

        public int Id
        {
            get { return Order.Id; }
            set
            {
                Order.Id = value;
                NotifyPropertyChanged("Id");
            }
        }

        public string CustomerName
        {
            get { return Order.CustomerName; }
            set
            {
                Order.CustomerName = value;
                NotifyPropertyChanged("CustomerName");
            }
        }

        public int HolidayId
        {
            get { return Order.HolidayId; }
            set
            {
                Order.HolidayId = value;
                NotifyPropertyChanged("HolidayId");
            }
        }

        public int OrderNumber
        {
            get { return Order.OrderNumber; }
            set
            {
                Order.OrderNumber = value;
                NotifyPropertyChanged("OrderNumber");
            }
        }

        public string Comments
        {
            get { return Order.Comments; }
            set
            {
                Order.Comments = value;
                NotifyPropertyChanged("Comments");
            }
        }

        public string Phone
        {
            get { return Order.Phone; }
            set
            {
                Order.Phone= value;
                NotifyPropertyChanged("Phone");
            }
        }

        public virtual Holiday Holiday
        {
            get { return Order.Holiday; }
            set
            {
                Order.Holiday= value;
                NotifyPropertyChanged("Holiday");
            }
        }

        public virtual ICollection<Item> Items
        {
            get { return Order.Items; }
            set
            {
                Order.Items = value;
                NotifyPropertyChanged("Items");
            }
        }

        public bool IsVisible
        {
            get { return _isVisible; }
            set
            {
                if (IsVisible != value)
                {
                    _isVisible = value;
                    NotifyPropertyChanged("IsVisible");
                }
            }
        }

        public OrderWrapper(Order order)
        {
            Order= order;
            IsVisible = true;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string info)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(info));
        }
    }
}
