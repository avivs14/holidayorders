﻿/************************************************************************
 * Copyright: Hans Wolff
 *
 * License:  This software abides by the LGPL license terms. For further
 *           licensing information please see the top level LICENSE.txt 
 *           file found in the root directory of CodeReason Reports.
 *
 * Author:   Hans Wolff
 *
 ************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.IO.Packaging;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Xps.Packaging;
using CodeReason.Reports;
using MahApps.Metro.Controls;
using System.Threading.Tasks;

namespace HolidayOrders.UI
{
    /// <summary>
    /// Application's main form
    /// </summary>
    public partial class PrintBakerReport : MetroWindow
    {
        internal static Settings Config = Settings.Default;

        public string FilePath { get; set; }
        private readonly string _holidayName;
        private bool _firstActivated = true;
        private readonly IList<SummeryRow> _summeryRows;

        /// <summary>
        /// Constructor
        /// </summary>
        public PrintBakerReport(IList<SummeryRow> summeryRows, string holidayName)
        {
            _summeryRows = summeryRows;
            _holidayName = holidayName;
            InitializeComponent();
        }

        /// <summary>
        /// Window has been activated
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event details</param>
        private void RemoveOldFiles()
        {
            var v = Directory.GetFiles("".AddCurrentDirectory());
            v.Where(x => x.Contains("tmp") || x.Contains("סיכום")).ToList().ForEach(x =>
            {
                try
                {
                    File.Delete(x);
                }
                catch (Exception)
                {
                }
            });
        }

        public async Task ActivateWindow()
        {
            if (!_firstActivated) return;

            _firstActivated = false;


            await Task.Run(async () =>
            {
                try
                {
                    RemoveOldFiles();

                    var allXps = new List<XpsDocument>();
                    var db = DB.Instance;
                    if (_summeryRows.Any())
                    {
                        var xps = await CreateXps(_summeryRows, _holidayName);
                        allXps.Add(xps);


                        FilePath =
                            Path.ChangeExtension("סיכום" + Path.GetFileName(Path.GetTempFileName()), ".xps")
                                .AddCurrentDirectory();


                        await Application.Current.Dispatcher.InvokeAsync(() =>
                        {
                            allXps.MergeXpsDocumentAsync(FilePath);
                            var mergedXpsDocument = new XpsDocument(FilePath, FileAccess.ReadWrite);
                            documentViewer.Document = mergedXpsDocument.GetFixedDocumentSequence();
                            mergedXpsDocument.CleanPackageFromMemory();

                        });
                    }
                }
                catch (Exception ex)
                {
                    // show exception
                    MessageBox.Show("ארעה תקלה בעת הכנת התצוגה המקדימה להדפסה", "אופס",
                        MessageBoxButton.OK, MessageBoxImage.Stop);
                }
            });
        }

        private static async Task<XpsDocument> CreateXps(IList<SummeryRow> summeryRows, string holidayName)
        {
            ReportDocument reportDocument = new ReportDocument();

            StreamReader reader =
                new StreamReader(new FileStream(Config.BakerReportTemplatePath, FileMode.Open, FileAccess.Read));
            reportDocument.XamlData = reader.ReadToEnd();
            reportDocument.XamlImagePath = Path.Combine(Environment.CurrentDirectory, Config.TemplateBasePath);
            reader.Close();

            ReportData data = new ReportData();

            // set constant document values
            data.ReportDocumentValues.Add("PrintDate", DateTime.Now); // print date is now
            data.ReportDocumentValues.Add("ReportTitle", "סיכום לטבח - " + holidayName);
            // sample table "Ean"
            DataTable table = new DataTable("Order");

            table.Columns.Add("Box4000", typeof(string));
            table.Columns.Add("Box3000", typeof(string));
            table.Columns.Add("Box2000", typeof(string));
            table.Columns.Add("Box1500", typeof(string));
            table.Columns.Add("Box1000", typeof(string));
            table.Columns.Add("Box750", typeof(string));
            table.Columns.Add("Box500", typeof(string));
            table.Columns.Add("Box250", typeof(string));

            table.Columns.Add("Units", typeof(string));
            table.Columns.Add("Weight", typeof(string));
            table.Columns.Add("FoodType", typeof(string));

            foreach (var summeryRow in summeryRows)
            {
                table.Rows.Add(
                    summeryRow.Box4000.ToDisplayUnits(),
                    summeryRow.Box3000.ToDisplayUnits(),
                    summeryRow.Box2000.ToDisplayUnits(),
                    summeryRow.Box1500.ToDisplayUnits(),
                    summeryRow.Box1000.ToDisplayUnits(),
                    summeryRow.Box750.ToDisplayUnits(),
                    summeryRow.Box500.ToDisplayUnits(),
                    summeryRow.Box250.ToDisplayUnits(),
                    summeryRow.TotalUnits.ToDisplayUnitsNulleble(),
                    summeryRow.TotalWeight.ToDisplayWeightNulleble(),
                    summeryRow.FoodType);
            }
            Random rnd = new Random(1234);

            data.DataTables.Add(table);

            var fileName =
                Path.ChangeExtension(Path.Combine(Path.GetFileName(Path.GetTempFileName()).AddCurrentDirectory()),
                    ".xps");
            XpsDocument xps = null;
            await
                Application.Current.Dispatcher.InvokeAsync(
                    () => { xps = reportDocument.CreateXpsDocument(data, fileName); });

            return xps;
        }
    }
}
