﻿/************************************************************************
 * Copyright: Hans Wolff
 *
 * License:  This software abides by the LGPL license terms. For further
 *           licensing information please see the top level LICENSE.txt 
 *           file found in the root directory of CodeReason Reports.
 *
 * Author:   Hans Wolff
 *
 ************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.IO.Packaging;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Threading.Tasks;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Xps.Packaging;
using CodeReason.Reports;
using MahApps.Metro.Controls;

namespace HolidayOrders.UI
{
    /// <summary>
    /// Application's main form
    /// </summary>
    public partial class PrintOrdersWindow : MetroWindow
    {

        internal static Settings Config = Settings.Default;
        public string FilePath { get; set; }
        private bool _firstActivated = true;
        private IList<Order> _orders;
        /// <summary>
        /// Constructor
        /// </summary>
        public PrintOrdersWindow(IList<Order> orders)
        {
            _orders = orders;
            InitializeComponent();
        }

        /// <summary>
        /// Window has been activated
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event details</param>
        private void RemoveOldFiles()
        {
            var v = Directory.GetFiles("".AddCurrentDirectory());
            v.Where(x => x.Contains("tmp") || x.Contains("סיכום")).ToList().ForEach(x =>
            {
                try
                {
                    File.Delete(x);
                }
                catch (Exception)
                {
                }
            });
        }
        public async Task ActivateWindow()
        {
            if (!_firstActivated) return;

            _firstActivated = false;



            await Task.Run(async () =>
             {
                 try
                 {
                     RemoveOldFiles();

                     var allXps = new List<XpsDocument>();
                     var db = DB.Instance;
                     if (_orders.Any())
                     {
                         foreach (var order in _orders)
                         {
                             var xps = CreateXps(order);
                             allXps.Add(xps);
                         }


                         FilePath = Path.ChangeExtension("סיכום" + Path.GetFileName(Path.GetTempFileName()), ".xps").AddCurrentDirectory();

                         await Application.Current.Dispatcher.InvokeAsync(() =>
                         {
                             allXps.MergeXpsDocumentAsync(FilePath);

                             var mergedXpsDocument = new XpsDocument(FilePath, FileAccess.ReadWrite);
                             documentViewer.Document = mergedXpsDocument.GetFixedDocumentSequence();
                             mergedXpsDocument.CleanPackageFromMemory();

                         });

                     }
                 }
                 catch (Exception ex)
                 {
                     // show exception
                     MessageBox.Show("ארעה תקלה בעת הכנת התצוגה המקדימה להדפסה", "אופס",
                          MessageBoxButton.OK, MessageBoxImage.Stop);
                 }

             });

        }

        private static XpsDocument CreateXps(Order order)
        {
            ReportDocument reportDocument = new ReportDocument();

            StreamReader reader = new StreamReader(new FileStream(Config.OrderReportTemplatePath, FileMode.Open, FileAccess.Read));
            reportDocument.XamlData = reader.ReadToEnd();
            reportDocument.XamlImagePath = Path.Combine(Environment.CurrentDirectory, Config.TemplateBasePath);
            reader.Close();

            ReportData data = new ReportData();

            // set constant document values

            data.ReportDocumentValues.Add("Greeting", Config.OrderReportGreeting);
            data.ReportDocumentValues.Add("PrintDate", DateTime.Now); // print date is now
            data.ReportDocumentValues.Add("ReportTitle", "סיכום הזמנה - " + order.CustomerName + " - " + order.Holiday.Name);
            // sample table "Ean"
            DataTable table = new DataTable("Order");

            table.Columns.Add("Units", typeof(string));
            table.Columns.Add("Ammount", typeof(string));
            table.Columns.Add("Item", typeof(string));
            table.Columns.Add("Number", typeof(string));

            var index = 1;
            foreach (var item in order.Items)
            {
                string ammount;
                string units;


                if (item.IsUnits)
                {
                    ammount = item.Units.ToString("N0");
                    units = "יחידות";
                }
                else
                {
                    var isBoxOf = item.IsBoxOf ? "קופסא של " : string.Empty;
                    ammount = isBoxOf + item.Box.DisplayWeight;
                    units = item.Box.DisplayUnits;
                }
                table.Rows.Add(units, ammount, item.Food.Name, index);
                index++;

            }
            Random rnd = new Random(1234);

            data.DataTables.Add(table);
            data.ReportDocumentValues.Add("OrderNumber", "מספר הזמנה : " + order.Holiday.Symbol + order.OrderNumber);

            string commentsLine = "";
            if (!string.IsNullOrEmpty(order.Comments))
            {
                commentsLine = "הערות : " + order.Comments;
            }
            data.ReportDocumentValues.Add("Comments", commentsLine);

            string phoneLine = "";
            if (!string.IsNullOrEmpty(order.Phone))
            {
                commentsLine = "פלאפון : " + order.Phone;
            }

            data.ReportDocumentValues.Add("Phone", commentsLine);


            var fileName =
                Path.ChangeExtension(Path.Combine(Path.GetFileName(Path.GetTempFileName()).AddCurrentDirectory()),
                    ".xps");
            var xps = reportDocument.CreateXpsDocument(data, fileName);
            var xpsPath = fileName;

            return xps;
        }
    }
}
