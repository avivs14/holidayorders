﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace HolidayOrders.UI
{
    public class ItemToIndexConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType,
             object parameter, System.Globalization.CultureInfo culture)
        {
            var item = (Item)values[0];
            var Items = (ObservableCollection<ItemWrapper>)values[1];
            var sameItem = Items.FirstOrDefault(x => x.Item.Id == item.Id);
            if (sameItem != null)
            {
                return (Items.IndexOf(sameItem) + 1).ToString();
            }
            return "";
        }
        public object[] ConvertBack(object value, Type[] targetTypes,
               object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException("Cannot convert back");
        }
    }
}