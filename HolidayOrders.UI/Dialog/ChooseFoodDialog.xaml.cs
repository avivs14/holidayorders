﻿using System.Windows.Controls;

namespace HolidayOrders.UI.Dialogs
{
    /// <summary>
    /// Interaction logic for CustomDialogContent.xaml
    /// </summary>
    public partial class MessageDialog : UserControl
    {
        public MessageDialog()
        {
            InitializeComponent();
        }
    }
}
