﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Input;
using FoodWay.GUI.BaseClasses;
using Prism.Commands;

namespace HolidayOrders.UI.Dialogs
{
    public class MessageDialogContent : Notifier
    {
        private string _message;

        public MessageDialogContent(string message)
        {
            Message = message;
        }

        public string Message
        {
            get { return _message; }
            set
            {
                if (value == _message) return;
                _message = value;
                OnPropertyChanged();
            }
        }
    }
}

