﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Annotations;
using System.Windows.Controls;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using MonthSummery.UI;
using Prism.Commands;

namespace HolidayOrders.UI
{
    public class EditFoodPresenter : INotifyPropertyChanged
    {
        private ObservableCollection<Food> _allFoods;
        private Food _selectedFood;

        public Food SelectedFood
        {
            get { return _selectedFood; }
            set
            {
                _selectedFood = value;
                NotifyPropertyChanged("SelectedFood");
            }
        }

        public DelegateCommand DeleteFoodCommand { get; set; }
        public ObservableCollection<Food> AllFoods
        {
            get { return _allFoods; }
            set
            {
                _allFoods = value;
                NotifyPropertyChanged("AllFoods");
            }
        }

        public DelegateCommand AddFoodCommand { get; set; }
        public DelegateCommand FoodNameChangedCommand { get; set; }


        public EditFoodPresenter()
        {
            AllFoods = new ObservableCollection<Food>(DB.Instance.Foods.ToList());

            AddFoodCommand = new DelegateCommand(AddFood);
            FoodNameChangedCommand = new DelegateCommand(FoodNameChanged);
            DeleteFoodCommand = new DelegateCommand(DeleteFood);
        }

        private void FoodNameChanged()
        {
            var db = DB.Instance;
            var foodOrNull = db.Foods.FirstOrDefault(x => x.Id == SelectedFood.Id);
            if (foodOrNull != null)
            {
                foodOrNull.Name = SelectedFood.Name;
            }
            DB.Save(db);
        }

        private void AddFood()
        {
            var db = DB.Instance;
            var newFood = new Food() { HolidayId = 0, Name = "פריט חדש" };
            db.Foods.Add(newFood);
            DB.Save(db);
            AllFoods.Add(newFood);
            var metroWindow = Application.Current.Windows.Cast<Window>().First(x => x is EditFoodWindow) as EditFoodWindow;
            var foodGrid =metroWindow.FindVisualChildByName<DataGrid>("foodDataGrid");
            foodGrid.ScrollIntoView(foodGrid.Items[foodGrid.Items.Count-1]);
            foodGrid.SelectedIndex = foodGrid.Items.Count - 1;

        }

        private async void DeleteFood()
        {
            if (SelectedFood.Name == "בחר פריט" && AllFoods.Count(x=>x.Name=="בחר פריט")<2)
            {
                MessageBox.Show("חובה להשאיר פריט אחד בשם בחר פריט");
                
            }
            else
            {
                var metroWindow = Application.Current.Windows.Cast<Window>().First(x => x is EditFoodWindow) as MetroWindow;
                var settings = new MetroDialogSettings();
                settings.NegativeButtonText = "כן, אני בטוח";
                settings.AffirmativeButtonText = "ביטול";
                var result = await metroWindow.ShowMessageAsync("האם אתה בטוח?",
                    "מחיקה של המאכל תמחק את כל ההזמנות וכל הפריטים שהוא נמצא בהם!, האם אתה בטוח שברצונך למחוק מאכל זה ?",
                    MessageDialogStyle.AffirmativeAndNegative, settings);

                if (result == MessageDialogResult.Negative)
                {
                    int selectedIndex = AllFoods.IndexOf(SelectedFood);

                    BackupManager.BackupLocal();

                    var db = DB.Instance;
                    var foodOrNull = db.Foods.FirstOrDefault(x => x.Id == SelectedFood.Id);
                    if (foodOrNull != null)
                    {
                        db.Foods.Remove(foodOrNull);
                    }

                    if (AllFoods.Any())
                    {
                        //if (selectedIndex > 0)
                        //{
                        //    selectedIndex--;
                        //}
                        SelectedFood = AllFoods[selectedIndex];
                    }

                    DB.Save(db);
                    AllFoods.Remove(SelectedFood);
                }
            }

    
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string info)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(info));
        }
    }
}
