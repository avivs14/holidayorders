﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dropbox.Api;
using Dropbox.Api.FileRequests;
using HolidayOrders.UI;

namespace HolidayOrders.Web.Controllers
{
    public class GetAllOrdersController : ApiController
    {
        [Route("GetAllOrders")]
        [HttpGet]
        public MyOrder[] Get()
        {
            DropboxClient client1 = new DropboxClient("_0OmS47-YQEAAAAAAAAmeYsI4jYNwo9NhCqmcQPIK2pK-Dy4yoekSYKDtGHW_vLM");
            var rootFolder = client1.Files.ListFolderAsync(@"/HolidayDB").Result;
            var HolidayDBSdfFile = rootFolder.Entries.FirstOrDefault(x => x.Name == "HolidayDB.sdf");
            byte[] fileBytes = null;
            if (HolidayDBSdfFile != null)
            {

                using (var response = client1.Files.DownloadAsync(HolidayDBSdfFile.PathDisplay).Result)
                {
                    fileBytes = response.GetContentAsByteArrayAsync().Result;
                }
            }

            var tempDirectory = Path.GetTempPath();
            var outPutPath = Path.Combine(tempDirectory, HolidayDBSdfFile.Name);
            if (File.Exists(outPutPath))
            {
                File.Delete(outPutPath);
            }

            using (FileStream fs = new FileStream(outPutPath, FileMode.Create))
            {
                fs.Write(fileBytes, 0, fileBytes.Length);
            }
            return HolidayOrders.UI.DB.Instance.Orders
                .Select(x => new MyOrder()
                {
                    HolidayID = x.Holiday.Id.ToString(),
                    Holiday = x.Holiday.Name,
                    Name = x.CustomerName,
                    Phone = x.Phone,
                    Comments = x.Comments,
                    OrderId = x.Id.ToString(),
                    OrderNumber = x.Holiday.Symbol + x.OrderNumber
                })
                 .ToArray();
        }


        [Route("GetItemsFor/{orderId}")]
        [HttpGet]
        public MyItem[] GetItmes(int orderId)
        {

            var orderOrNull = HolidayOrders.UI.DB.Instance.Orders.SingleOrDefault(x => x.Id == orderId);
            if (orderOrNull != null)
            {
                return orderOrNull.Items.Select(x =>
                {
                    var toReturn = new MyItem() { Food = x.Food.Name };
                    if (x.IsUnits)
                    {
                        toReturn.Ammount = x.Units.ToDisplayUnits();
                        toReturn.Units = "יחידות";
                    }
                    else
                    {
                        toReturn.Ammount = x.Box.DisplayWeight;
                        toReturn.Units = x.Box.DisplayUnits;
                    }
                    return toReturn;

                    {
                    }
                }).ToArray();
            }
            return Array.Empty<MyItem>();
        }


        [Route("GetOrders/{holidayID}")]
        [HttpGet]
        public MyOrder[] Get(int holidayID)
        {

            var holiday = HolidayOrders.UI.DB.Instance.HolidaySet.SingleOrDefault(x => x.Id == holidayID);
            if (holiday != null)
            {
                return holiday.Orders.Select(x => new MyOrder() { Holiday = x.Holiday.Name, Name = x.CustomerName, OrderNumber = x.Holiday.Symbol + x.OrderNumber })
                .ToArray();
            }
            return Array.Empty<MyOrder>();

        }

        [Route("GetAllHolidays")]
        [HttpGet]
        public MyHoliday[] GetHolidays()
        {
            return DB.Instance.HolidaySet.Select(x => new MyHoliday() { Id = x.Id.ToString(), Name = x.Name }).ToArray();
        }
    }

    public class MyOrder
    {
        public string OrderId { get; set; }
        public string Phone { get; set; }
        public string Comments { get; set; }
        public string Name { get; set; }
        public string Holiday { get; set; }
        public string HolidayID { get; set; }
        public string OrderNumber { get; set; }
    }
    public class MyHoliday
    {
        public string Name { get; set; }
        public string Id { get; set; }
    }

    public class MyItem
    {

        public int OrderId { get; set; }
        public string Units { get; set; }
        public string Ammount { get; set; }
        public string Food { get; set; }

    }
}
